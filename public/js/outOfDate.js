window.showHide= function() {
    var chkBox = document.getElementById("isOOD");
    var txtBox = document.getElementById("newComment");
    var submit = document.getElementById("Submit");

    if (chkBox.checked) {
        txtBox.style.visibility = "visible";
        submit.style.visibility = "visible";
    } else {
        txtBox.style.visibility = "hidden";
        submit.style.visibility = "hidden";
    }
}