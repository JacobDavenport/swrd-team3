$(function() {
    $("#notificationToggle").click(function () {
       $("#notificationToggle").toggleClass("toggleShowButton");
        $("#notificationContainer").toggleClass("shrinkContainer");
        if($("#notificationContainer .row").css('display') == 'none')
        {
            $("#notificationContainer .row").css("visibility","visible");
            $("#notificationContainer .row").show();
        }
        else {
            $("#notificationContainer .row").css("visibility","hidden");
            setTimeout(function(){$("#notificationContainer .row").hide(); },1000)

        }

    });
})