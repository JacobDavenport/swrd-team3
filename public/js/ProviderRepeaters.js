/**
 * Created by travis on 11/10/16.
 */
var idxPhone = 0;       //Maintains the last used index of the phone collection; the collection may be sparsely populated
var idxResource = 0;    //As idxPhone, but with resources

function fixIndex(index, containerID){
    var count = $("#" + containerID).children().length;
    return (count > index) ? count : index;
}

function addPhone(containerID) {

    //The edit page drops the proper index value after page load, so fix that.      \'number\'      \'note\'
    idxPhone = fixIndex(idxPhone, containerID);

    var mdlPhone =
        '<div id="phone_' + idxPhone + '" >' +//idxPhone + '" >' +
            '<div class="form-group">' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="phoneNumber_' + idxPhone + '">Phone</label>' +
                '<div class="col-md-6 col-sm-6 col-xs-12">' +
                    '<input id="phoneNumber_' + idxPhone + '" name="phoneNumber[]" placeholder = "Enter Phone" class="form-control col-md-7 col-xs-12" value = "">' +
                '</div>' +
            '</div>' +
            '<div class="form-group">' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="phoneNote_' + idxPhone + '">Note</label>' +
                '<div class="col-md-6 col-sm-6 col-xs-12">' +
                    '<input type="text" id="phoneNote_' + idxPhone + '" name="phoneNote[]" placeholder = "Enter Note" class="form-control col-md-7 col-xs-12">' +
                '</div>' +
            '</div>' +
            '<div class="form-group">' +
                '<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">' +
                    '<input type="button" class="btn btn-primary" value="Remove Phone" onClick="removeGroup(\'phone_' + idxPhone +'\')" />' +
                '</div>' +
            '</div>' +
        '</div>';

    $('#' + containerID).append(mdlPhone);

    return idxPhone++;
}

function fillPhone(fldPhone, phoneNumber, phoneNote){
    $("#phoneNumber_" + fldPhone).val(phoneNumber);
    $("#phoneNote_" + fldPhone).val(phoneNote);
}

function addResource(containerID, resourceContents){

    //The edit page drops the proper index value after page load, so fix that.
    idxResource = fixIndex(idxResource, containerID);

    var mdlResource =
        '<div id="grpResource_' + idxResource + '" >' +
            '<div class="form-group">' +
                '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="resource_'+ idxResource +'">Resource Type</label>' +
                '<div class="col-md-4 col-sm-4 col-xs-12">' +
                    '<select required="required" id="resource_'+ idxResource +'" name="resource[]" class="form-control col-md-7 col-xs-12">' +
                        resourceContents +
                    '</select>' +
                '</div>' +
                '<div class="col-md-2 col-sm-2 col-xs-12">' +
                    '<input type="button" class="btn btn-primary" value="Remove Resource" onClick="removeGroup(\'grpResource_' + idxResource +'\')" />' +
                '</div>' +
            '</div>' +
        '</div>';

    $('#' + containerID).append(mdlResource);

    return idxResource++;
}

function selectResource(fldResource, resourceID){
    $("#resource_" + fldResource).val(resourceID);
}

function removeGroup(id){
    $('#' + id).remove();
}