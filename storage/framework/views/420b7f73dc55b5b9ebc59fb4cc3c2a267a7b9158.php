<?php $__env->startSection('title'); ?>
Providers List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
<?php


    function printProvider($provider)
    {
        echo "<td> $provider->name </td>";

        $formattedPhone = $provider->formatPhone();

        echo "<td> $formattedPhone </td>";
        echo "<td> ";
            foreach($provider->resources as $resource)
            {
                echo $resource->type;
                echo " - ";
                echo $resource->subtype;
                echo "</br>";
            }
        echo " </td>";
        echo "<td> $provider->county </td>";
        echo "<td> <a href='/providers/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> View </a>";
        if (Auth::user()->hasRole('GA'))
        {
            echo " <a href='/providers/edit/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Edit </a>";
        }
        if (Auth::user()->hasRole('admin'))
        {
            echo " <a href='/providers/archive/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Delete </a>";
        }
        echo "</td>";
        echo "<td><a ><button type=\"button\" onclick=addToCart($provider->id) class=\"btn btn-round btn-info\"><i class=\"fa fa-plus\"></i></button></a></td>";


    }
?>

<?php if(Auth::user()->hasRole('GA')): ?>


 
<?php endif; ?>
<div id="flashDiv" class="clearfix">
    <br><br>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
  <div class="">


     <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Providers</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <table id="datatable" class="table table-striped nowrap">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Phone number</th>
                  <th>Resources</th>
                  <th>County</th>
                  <th>Settings</th>
                  <th>Add to Cart</th>
                </tr>
              </thead>
             <tbody>
             <?php foreach($providers as $provider): ?>
                 <tr>
                     <?php printProvider($provider); ?>
                 </tr>
             <?php endforeach; ?>
       </tbody>
      </table>
     </div>
    </div>
  </div>
 </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<!-- Datatables -->
<!-- this is not the way this is supposed to be done. php tags in a view is frowned upon but it works -->
    <?php
    if(isset($searchTerm))
    {
        echo "<script>$('#datatable').dataTable({\"search\": {\"search\": \"$searchTerm\"}});</script>";
    }
    else
    {
        echo "<script>$('#datatable').dataTable();</script>";
    }
    ?>



<script src="/js/addToCart.js"></script>
    <script src="/js/alertFade.js"></script>
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- /PNotify -->

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>