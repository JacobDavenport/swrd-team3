

<?php $__env->startSection('title'); ?>

providerEdit

<?php
    echo $provider->name;
?>
<?php $__env->stopSection(); ?>
<!--
This section needs to be refactored to match the other pages layout, and design.
-->
<?php $__env->startSection('content'); ?>
<!--
This is copied from the User Edit page to make sure that is looks the same. It will need to be changed for the things in the provider class.
-->
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Edit Provider</h2>                
      <br>       
    </div>           
    <?php echo Form::open(array('action' => array('ProviderController@updatesProvider',$provider->id), 'class' => 'form-horizontal form-label-left')); ?>    
    <br>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="hidden" id="id"  value = "<?php echo e($provider->id); ?>" required="required" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="name" name="name" value = "<?php echo e($provider->name); ?>" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street">Street 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="street" name="street" value = "<?php echo e($provider->street); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="city" name="city" value = "<?php echo e($provider->city); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="county">County  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="county" name="county" value = "<?php echo e($provider->county); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zip">Zip  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="zip" name="zip" value = "<?php echo e($provider->zip); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">State 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="state" name="state" value = "<?php echo e($provider->state); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

    <div id="phoneContainer" class="form-group"></div>
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
        <input type="button" class="btn btn-primary" value="Add Phone" onClick="addPhone('phoneContainer')" />
      </div>
    </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="email" name="email" value = "<?php echo e($provider->email); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="web">Web Address 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="web" name="web" value = "<?php echo e($provider->web); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_f_name">Contact First Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="contact_f_name" name="contact_f_name" value = "<?php echo e($provider->contact_f_name); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_l_name">Contact Last Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="contact_l_name" name="contact_l_name" value = "<?php echo e($provider->contact_l_name); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="location" name="location" value = "<?php echo e($provider->location); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="population">Population  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="population" name="population" value = "<?php echo e($provider->population); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="office_hours">Office Hours  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="office_hours" name="office_hours" value = "<?php echo e($provider->office_hours); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="flag_archived">Archived  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="flag_archived" name="flag_archived" value = "<?php echo e($provider->flag_archived); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="flag_out_of_date">Out of Date  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="flag_out_of_date" name="flag_out_of_date" value = "<?php echo e($provider->flag_out_of_date); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" value = "<?php echo e($provider->description); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="intake">Intake  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="intake" name="intake" value = "<?php echo e($provider->intake); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="feed">Fees  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="fees" name="fees" value = "<?php echo e($provider->fees); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div id="resourceContainer" class="form-group"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <input type="button" class="btn btn-primary" value="Add Resource" onClick="addResource('resourceContainer', resourceContents)" />
        </div>
      </div>
            
      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-success" name="save" value="Save Changes">
            <input type="submit" class="btn btn-danger" name="delete" value="Delete">
            <input type="submit"  class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

<?php echo Form::close(); ?>


  <?php $__env->startSection('scripts'); ?>
    <script src="/js/ProviderRepeaters.js"></script>

    <script>
      //Setup the repeating fields using data from the database

      //Setup the resources combo box data
      var resourceContents =
              <?php
                      foreach($resources as $resource)
                      {
                        echo "'<option value=\"" . $resource->id . "\">$resource->subtype</option>' +";
                      }
                      ?>
                      '';

      if($("#phoneContainer").children().length == 0){
        //Display the existing phones
        var idx = 0;
        <?php
        foreach($provider->phones as $p)
        {
          echo "idx = addPhone('phoneContainer');\n";
          echo "fillPhone(idx, '$p->phone', '$p->note')\n";
        }
        ?>
      }

      if($("#resourceContainer").children().length == 0){
        //Display existing resources
        var idx = 0
        <?php
        foreach($provider->resources as $r)
        {
          echo "idx = addResource('resourceContainer', resourceContents);\n";
          echo "selectResource(idx, '$r->id')\n";
        }
        ?>
      }

    </script>


  <?php $__env->stopSection(); ?>

    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>