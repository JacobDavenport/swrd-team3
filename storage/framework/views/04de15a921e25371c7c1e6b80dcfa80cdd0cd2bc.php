<?php $__env->startSection('head'); ?>
    <!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="clearfix"><br><br>  <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
     <div class="row">


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Cart</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

          <table id="datatable" class="table table-striped nowrap">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Phone number</th>
                  <th>Remove from Cart</th>
                </tr>
              </thead>
             <tbody>
             <?php if(isset($providerArray)): ?>
             <?php foreach($providerArray as $pdfitem): ?>
                 <tr>
                    <td>
                        <?php echo $pdfitem->name; ?>
                    </td>
                    <td>
                        <?php echo $pdfitem->description; ?>
                    </td>
                    <td>
                        <?php echo $pdfitem->formatPhone(); ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo "<a href='/cart/remove/$pdfitem->id'<button type=\"button\" class=\"btn btn-round btn-info\"><i class=\"fa fa-minus\"></i></button></a>"; ?>
                    </td>
                 </tr>
             <?php endforeach; ?>
             <?php else: ?>
             <tr><td><b>Cart is empty</b></td></tr>
             <?php endif; ?>
       </tbody>
      </table>
      <table>
        <tr>
            <td>
                <a href="/cart/clearCart" class="btn btn-primary btn-xs">Clear Cart</a>
            </td>
            <td>
                <a onclick=popUpWindow() class="btn btn-primary btn-xs">Print Cart</a>
            </td>
        </tr>
      </table>
     </div>
    </div>
  </div>
 </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

   <script src="/js/alertFade.js"></script>
    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- /PNotify -->
 <script>
        function popUpWindow(){window.open("/cart/PDF", "Cart Print View");}
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>