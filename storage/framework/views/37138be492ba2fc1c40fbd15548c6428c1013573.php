

<?php $__env->startSection('title'); ?>
<?php
echo $provider->name;
?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="clearfix"><br><br><?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
    <div><a href="<?php echo e(\Illuminate\Support\Facades\URL::previous()); ?>" class="btn btn-primary btn-xs" >back</a></div>


    <table class="table table-hover" style="border: 1px solid #ddd;">
       <tr style="border: 1px solid #ddd;">
           <th style="border: 1px solid #ddd;">Name</th>
           <th style="border: 1px solid #ddd;">Address</th>
           <th style="border: 1px solid #ddd;">County</th>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->name); ?></td>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->street); ?>, <?php echo e($provider->city); ?>, <?php echo e($provider->state); ?> <?php echo e($provider->zip); ?></br>
                </td>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->county); ?></td>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd;">Phone Number</th>
            <th style="border: 1px solid #ddd;">Email</th>
            <th style="border: 1px solid #ddd;">Web Address</th>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->formatPhone()); ?></td>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->email); ?></td>
            <td style="border: 1px solid #ddd;"> <?php echo e($provider->web); ?></td>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd;">Point of Contact</th>
            <th style="border: 1px solid #ddd;">Area of Service</th>
            <th style="border: 1px solid #ddd;">Service Restriction</th>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->contact_f_name); ?> <?php echo e($provider->contact_l_name); ?></td>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->location); ?></td>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->population); ?></td>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd;">Hours</th>
            <th style="border: 1px solid #ddd;">Has this resource been archived?</th>
            <th style="border: 1px solid #ddd;">Is this resource flagged as outdated?</th>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->office_hours); ?></td>
            <td style="border: 1px solid #ddd;"><?php echo e($provider->flag_archived); ?></td>
            <td style="border: 1px solid #ddd;"> <?php echo e($provider->flag_out_of_date); ?></td>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th>Description</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <td><?php echo e($provider->description); ?></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>

    </table>
    <table class="table table-hover" style="border: 1px solid #ddd;">
        <tr>
            <th>Resources</th>
            <th></th>
            <th></th>
        </tr>
        <tr>
            <th>Type</th>
            <th>Sub-type</th>
            <th>Description</th>
        </tr>

        <?php
            foreach($provider->resources as $r){
                echo "<tr>";
                echo "<td>$r->type</td>";
                echo "<td>$r->subtype</td>";
                echo "<td>$r->description</td>";
                echo "</tr>";
            }
        ?>
    </table>

    <!-- load js file to toggle hidden form elements by checkbox-->

    <?php echo Form::open(array('action' => array('ProviderController@storeOOD'), 'class' => 'form-horizontal form-label-left')); ?>


         <input type="text" id="providerID" name="providerID" hidden="true" value="<?php echo e($provider->id); ?>">
        <input type="checkbox" id="isOOD" name="isOOD" onClick="showHide()" />Please flag out of date or incorrect content by checking this box.
        <br>

        <textarea name="newComment" id="newComment"  style="visibility: hidden;" placeholder="Please tell us about the problem" ></textarea>
        <br>
        <input id="Submit" name="Submit Correction" class="input-xlarge" style="visibility: hidden;" type="submit" placeholder="Submit Comment" /></form>


    <?php echo Form::close(); ?>




<?php
//////////////////////////////////////////////////////////////////////////////////
///CREATE TABLE SHOWING GA AND ABOVE USER COMMENTS ABOUT OUT OF DATE WEB CONTENT//
//////////////////////////////////////////////////////////////////////////////////

//if(Auth::user()->hasRole('GA') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('superAdmin'))
//{
   // module to display the comments for out of date procivders
  //     include(app_path('comments.php'));
//}//end Ga or above authorization.
if (Auth::user()->hasRole('GA') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('superAdmin'))
        {
            echo " <a href='/providers/edit/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Edit </a>";
        }

?>


<?php $__env->startSection('scripts'); ?>
    <script src="/js/outOfDate.js"></script>
    <script src="/js/AlertFade.js"></script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>"

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>