

<?php $__env->startSection('title'); ?>



<?php
    echo $resource->type;
?>
<?php $__env->stopSection(); ?>
<!--
This section needs to be refactored to match the other pages layout, and design.
-->
<?php $__env->startSection('content'); ?>
 
<!--
This is copied from the User Edit page to make sure that is looks the same. It will need to be changed for the things in the provider class.
-->
<div class="col-md-12 col-sm-12 col-xs-12">
       <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="x_panel">
    <div class="x_title">
      <h2>Edit Resource</h2>                
      <br>       
    </div>           
    <?php echo Form::open(array('action' => array('ResourceController@updateResource',$resource->id), 'class' => 'form-horizontal form-label-left')); ?>    
    <br>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">ID
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="id" value = "<?php echo e($resource->id); ?>" required="required" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="type" name="type" value = "<?php echo e($resource->type); ?>" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subtype">Subtype 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="subtype" name="subtype" value = "<?php echo e($resource->subtype); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" value = "<?php echo e($resource->description); ?>" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

    
            
      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-success" name="save" value="Save Changes">
            <input type="submit"  class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

<?php echo Form::close(); ?> 

    </div>
  </div>
  

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>