<?php $__env->startSection('title'); ?>
Resources
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
 

 <!-- page content -->
        

          <div class="">
           
          <div class="clearfix"><?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
             <div class="row">


              <div class="col-md-12 col-sm-12 col-xs-12">
           
                <div class="x_panel">
                <div class="x_title">
                    <h2>Resources</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                              <th style="width: 1%"></th>
                              <th style="width: 20%">Type</th>
                              <th>Sub-type</th>
                              <th>Description</th>
                              <th>Settings</th>
                            </tr>
                          </thead>
                         <tbody> 
                            <!-- Query the RESOURCE database table -->
                             <?php 
                                function printResource($resource)
                                {
                                    echo "<td> </td>";
                                    echo "<td> $resource->type </td>";
                                    echo "<td> $resource->subtype </td>";
                                    echo "<td> $resource->description </td>";
                                    //echo "<td><input type=\"checkbox\" name=\"Needs Attention\"></td>";
                                    //todo: remove first <a> tag below 
                                   // echo
                                   
                                      if (Auth::user()->hasRole('GA'))
                                      {
        
                                     echo   " <td> <a href='/resources/edit/$resource->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-pencil\"></i> Edit </a>";
                                        }
                                        
                                        if (Auth::user()->hasRole('admin'))
                                        {
        
                                     echo    "<a href='/resources/deletee/$resource->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-trash-o\"></i> Delete </a>";
                                        }
                                       echo "</td>";
                                }
                                    


                                
                            ?>
                              <!-- Loop through the db -->
                              <?php foreach($resources as $r): ?>
                                <tr>
                                    <?php printResource($r); ?>
                                </tr>
                              <?php endforeach; ?>
                           </tbody>
                          </table>
                         </div>
                        </div>
                      </div>
                     </div>
                    </div>

        <!-- /page content -->
    <!-- TODO: this next section needs to be in a resource.blade.php file 
         under the layouts folder instead of the following hard coded -->
<?php $__env->startSection('scripts'); ?>
    <script src="/js/alertFade.js"></script>
    

<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<!-- <?php echo e(Form::submit('Delete', array('class' => 'btn','id'=>'deletebtn', 'onclick' =>'return confirm("Are you sure want to delete?");'))); ?>-->

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>