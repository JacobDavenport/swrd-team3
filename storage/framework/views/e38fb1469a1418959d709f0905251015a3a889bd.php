<?php $__env->startSection('title'); ?>
Providers List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
<?php
     

    function printProvider($provider)
    {
        echo "<td> $provider->name </td>";
        
        $formattedPhone = $provider->formatPhone();

        echo "<td> $formattedPhone </td>";
        echo "<td> $provider->county </td>";
        echo "<td> ";
            foreach($provider->flagComments as $comment)
            {
                echo $comment->comment_text;
                echo "</br>";
            }
        echo " </td>";
        echo "<td> <a href='/providers/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> View </a>";
        echo " <a href='/providers/editflagged/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Edit </a>";
        if (Auth::user()->hasRole('GA'))
        {
            echo " <a href='/providers/unflag/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Clear Status </a> </td>";
        }
        
        
        }

?>


<div class="clearfix"><?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?></div>
  <div class="">

     <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Out of Date Providers</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <table id="datatable" class="table table-striped nowrap">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Phone number</th>
                  <th>County</th>
                  <th>Comments</th>
                  <th>Settings</th>
                </tr>
              </thead>
             <tbody>
             <?php foreach($providers as $provider): ?>
                 <tr>
                     <?php printProvider($provider); ?>
                 </tr>
             <?php endforeach; ?>
       </tbody>
      </table>
     </div>
    </div>
  </div>
 </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<!-- Datatables -->
<!-- this is not the way this is supposed to be done. php tags in a view is frowned upon but it works -->
    <?php
    if(isset($searchTerm))
    {
        echo "<script>$('#datatable').dataTable({\"search\": {\"search\": \"$searchTerm\"}});</script>";
    }
    else
    {
        echo "<script>$('#datatable').dataTable();</script>";
    }
    ?>

    <script src="/js/alertFade.js"></script>
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/vendors/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/vendors/pdfmake/build/vfs_fonts.js"></script>
<!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- /PNotify -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>