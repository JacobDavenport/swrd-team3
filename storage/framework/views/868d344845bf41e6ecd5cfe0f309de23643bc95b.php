

<?php $__env->startSection('title'); ?>
Temporary Events List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
<!-- PNotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
<?php
require(app_path('t_notifications.blade.php'));
?>

<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<br>
<br>
  <div class="">
  <div class="clearfix"></div>
     <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Temporary Events</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <table id="datatable" class="table table-striped nowrap">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Location</th>
                  <th>Expiration Date</th>
                  <th>Event Contact</th>
                  <th>Settings</th>
                </tr>
              </thead>
             <tbody>

                     <?php require(app_path('printNotification.php')); ?>

       </tbody>
      </table>
     </div>
    </div>
  </div>
 </div>
</div>
<?php $__env->startSection('scripts'); ?>
    <script src="/js/toggleDiv.js"></script>
    <script src="/js/AlertFade.js"></script>
    <script src="/vendors/jquery/dist/jquery.min.js"></script>

<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>