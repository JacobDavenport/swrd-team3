<?php $__env->startSection('title'); ?>
Add New Resource
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="x_panel">
    <div class="x_title">
      <h2>Add Resource</h2>                
      <br>       
    </div>           
    <?php echo Form::open(array('action' => array('ResourceController@addResource'), 'class' => 'form-horizontal form-label-left')); ?>    
    <br>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Type
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="type" name="type" placeholder = "Enter Type" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subtype">Subtype
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="subtype" name="subtype" placeholder = "Enter Subtype" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" placeholder = "Enter Description" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-success" name="save" value="Save">
            <input type="submit" formnovalidate class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

<?php echo Form::close(); ?> 

    </div>
  </div>
<?php $__env->startSection('scripts'); ?>
  <script src="/js/alertFade.js"></script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>