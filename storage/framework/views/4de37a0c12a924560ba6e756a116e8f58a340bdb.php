

<?php $__env->startSection('title'); ?>
<?php
    echo $user->name;
?>
<?php $__env->stopSection(); ?>
<!--
This section needs to be refactored to match the other pages layout, and design.
-->
<?php $__env->startSection('content'); ?>
<!--
This is copied from the User Edit page to make sure that is looks the same. It will need to be changed for the things in the provider class.
-->
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>My Profile</h2>                
      <br>       
    </div>           
    <?php echo Form::open(array('action' => array('ProfileController@viewProfile',$user->id), 'class' => 'form-horizontal form-label-left')); ?>    
    <br>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="name" name="name" value = "<?php echo e($user->name); ?>" required="required" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street">Email 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="email" name="email" value = "<?php echo e($user->email); ?>" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>  
<?php echo Form::close(); ?> 
        <form action='/profile/edit'><input type='submit' value='Edit'></form>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>