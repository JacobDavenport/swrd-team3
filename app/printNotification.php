<?php
foreach($notifications as $notification)
{
    echo "<tr>";
    echo "<td> $notification->name </td>";
    echo "<td> $notification->description </td>";
    $date = \Carbon\Carbon::createFromFormat('Y-m-d', $notification->date )->format('M j, Y');
    echo "<td> $date</td>";
    echo "<td> $notification->time </td>";
    echo "<td> $notification->location </td>";
    $date2 = \Carbon\Carbon::createFromFormat('Y-m-d', $notification->expiration_date )->format('M j, Y');
    echo "<td> $date2</td>";
    echo "<td> $notification->event_contact </td>";
    
    echo "<td> $notification->phone </td>";
    echo "<td>";
    if (Auth::user()->hasRole('GA'))
    {
        echo " <a href=" . url('temp/edit/' . $notification->id) . " class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Edit </a>";
    }
    if (Auth::user()->hasRole('admin'))
    {
        echo " <a href=" . url('temp/delete/' . $notification->id) . " class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Delete </a>";
    }
    echo "</td>";
    echo "</tr>";
}

