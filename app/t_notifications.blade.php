<?php

use Illuminate\Support\Facades\Cache;
/*So this is a psuedoview that gets called by the list providers page
Basically I have created a cache that stores all tempo notifications and loads then into the cache when
the homscreen controller welcome() is called. Because of this anytime a new notification is added it will need to
call the same code that populates the cache or redirect to the welcome() controller in homeComtroller.
@Chance : Basically I need some formatting done to make this look good. The one div that MUST remain on the outter layer is the
NotificationDiv. This is the div that javascript triggers to collapse on button press (js located in file that calls this. src=toggleDiv.js).
Anything placed outside of this div will not be hidden when the toggle button is pressed.
To create a notification view on any page, all that has to be done is to require this file inside the view you want
to include this view with.
*/
?>
<link href="{{asset('css/tempNotification.css')}}" rel="stylesheet">
<?php
if( \App\TempNotification::get()->count() > 0 )
{

    $count = \App\TempNotification::get()->count();

}
else
{
    $count = 0;
}
echo "<div id=\"notificationContainer\" class='shrinkContainer' >";
    //test to see if there are any temp events/notifications

    if($count >= 1 )
    {


            $loopCounter = 0;
            foreach (\App\TempNotification::get() as $displayItem )
            {
                $loopCounter = $loopCounter +1;
                $date0 = \Carbon\Carbon::createFromFormat('Y-m-d', $displayItem->expiration_date);

                    if($date0->isToday() || $date0->isPast())
                    {
                        $deleteMe = \App\TempNotification::find($displayItem->id);
                        $count = $count - 1;
                        $deleteMe->delete();
                        if($loopCounter == 1)
                            {
                                echo "<div  id=\"notificationToggle\" class=\"activeToggle\">";
                                echo "<p>Events&nbsp <span class=\"label badge-warning float-left\" >" . $count . "</span> " . "</p>";
                                echo "</div>";

                                echo "<div class=\"row\">";
                                echo "</div>";
                                echo "<div class=\"row\">";
                                echo "<div class=\"col-xs-3 col-sm-2\">";
                                echo "<p>Name</p>";
                                echo "</div>";
                                echo "<div class=\"col-xs-6 col-sm-2\">";
                                echo "<p>Description</p>";
                                echo "</div>";
                                echo "<div class=\"col-xs-3 col-sm-2\">";

                                echo "<p>Date</p>";
                                echo "</div>";
                                echo "<div class=\"col-md-2 hidden-sm hidden-xs\">";
                                echo "<p>Time</p>";
                                echo "</div>";
                                echo "<div class=\"hidden-xs col-sm-2\">";
                                echo "<p>Location</p>";
                                echo "</div>";

                                echo "</div>";
                            }

                    }
                    else
                    {

                        if($loopCounter == 1)
                        {
                            echo "<div  id=\"notificationToggle\" class=\"activeToggle\">";
                            echo "<p>Events&nbsp <span class=\"label badge-warning float-left\" >" . $count . "</span> " . "</p>";
                            echo "</div>";

                            echo "<div class=\"row\">";
                            echo "</div>";
                            echo "<div class=\"row\">";
                            echo "<div class=\"col-xs-3 col-sm-2\">";
                            echo "<p>Name</p>";
                            echo "</div>";
                            echo "<div class=\"col-xs-6 col-sm-2\">";
                            echo "<p>Description</p>";
                            echo "</div>";
                            echo "<div class=\"col-xs-3 col-sm-2\">";

                            echo "<p>Date</p>";
                            echo "</div>";
                            echo "<div class=\"col-md-2 hidden-sm hidden-xs\">";
                            echo "<p>Time</p>";
                            echo "</div>";
                            echo "<div class=\"hidden-xs col-sm-2\">";
                            echo "<p>Location</p>";
                            echo "</div>";
                            echo "<div class=\"hidden-xs col-sm-2\">";
                            echo "<p>Contact Number</p>";
                            echo "</div>";

                            echo "</div>";
                        }



                        echo "<div class=\"row\">";
                        echo "<div class=\"col-xs-3 col-sm-2\">";
                        echo "<p>$displayItem->name</p>";
                        echo "</div>";
                        echo "<div class=\"col-xs-6 col-sm-2\">";
                        echo "<p>$displayItem->description</p>";
                        echo "</div>";
                        echo "<div class=\"col-xs-3 col-sm-2\">";
                        $date1 = \Carbon\Carbon::createFromFormat('Y-m-d', $displayItem->date)->format('M j, Y');
                        echo "<p>$date1</p>";
                        echo "</div>";
                        echo "<div class=\"col-md-2 hidden-sm hidden-xs\">";
                        echo "<p>$displayItem->time</p>";
                        echo "</div>";
                        echo "<div class=\"hidden-xs col-sm-2\">";
                        echo "<p>$displayItem->location</p>";
                        echo "</div>";
                        echo "<div class=\"hidden-xs col-sm-2\">";
                        echo "<p>$displayItem->phone</p>";
                        echo "</div>";

                        echo "</div>";
                        echo "<br>";

                    }

            }





    }
    else
    {
        echo "<div  id=\"notificationToggle\" class=\"activeToggle\">";
        echo "<p>Events&nbsp <span class=\"label label-warning float-left\">" . $count . "</span> " . "</p>";

        echo "</div>";

        echo "<div class=\"row\">";
        echo "<div class=\"col-xs-7 col-sm-7\">";
        echo "<p>No events to display at this time.</p>";
        echo "</div>";
        echo "</div>";
    }
echo "</div>";



