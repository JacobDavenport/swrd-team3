<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempNotification extends Model
{
    protected $table = 'temp_notifications';
    protected $dateFormat = 'm,j,Y';

    public $timestamps = false;
}
