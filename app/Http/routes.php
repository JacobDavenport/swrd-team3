<?php

use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['auth']], function() {

    Route::get('/cart', 'HomeController@cart')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/notifications', 'HomeController@notifications')->middleware('role:GA', 'role:admin', 'role:superAdmin');

    Route::get('/', 'HomeController@welcome')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/home', 'HomeController@index')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/cart/remove/{provider}', 'HomeController@removeCartItem')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');

    Route::get('/providers', 'ProviderController@providerList')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/providers/{provider}', 'ProviderController@providerDetails')
        ->where('provider', '[0-9]+')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
        Route::get('/providers/archive', 'ProviderController@archiveProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
        Route::get('/providers/flagged', 'ProviderController@flaggedProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/providers/restoreArchive/{provider}', 'ProviderController@restoreArchive')
        ->where('provider', '[0-9]+')->middleware('role:admin', 'role:superAdmin');
        Route::get('/providers/{provider}/archiveFlag', 'ProviderController@archiveFlagProvider')
        ->where('provider', '[0-9]+')->middleware('role:admin', 'role:superAdmin');
    Route::get('/providers/add', 'ProviderController@addProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/providers/add', 'ProviderController@storeProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/providers/editflagged/{provider}','ProviderController@editflaggedProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/providers/editflagged/{provider}', 'ProviderController@updatesProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/providers/edit/{provider}','ProviderController@editProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/providers/edit/{provider}', 'ProviderController@updateProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/providers/{searchTerm}', 'ProviderController@searchProvider')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/providers/ood', 'ProviderController@storeOOD')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    //Returns the user to the previous page after adding an item to their cart.
    Route::get('/providers/addToCart/{provider}',  'ProviderController@addToCart')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/providers/archive/{provider}', 'ProviderController@archive')->middleware('role:admin', 'role:superAdmin');
    Route::get('/providers/unflag/{provider}', 'ProviderController@unflag')->middleware('role:admin', 'role:superAdmin');
    Route::get('/cart/PDF', 'ProviderController@PDF')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/cart/clearCart', 'ProviderController@clearCart')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');



    //Route::get('/cart/remove/{provider}', 'ProviderController@removeCartItem');




    Route::get('/resource', 'ResourceController@resourceList')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/add', 'ResourceController@addResource')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/resources/add', 'ResourceController@storeResource')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/edit/{resource}','ResourceController@editResource')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/{resource}/edit','ResourceController@editResource')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/link','ResourceController@linkProvider')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/resources/link','ResourceController@storeLink')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/delete/{resource}','ResourceController@resourceDelete')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/delete_/{resource}','ResourceController@deleteResource')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/resources/edit/{resource}', 'ResourceController@updateResource')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/resources/{resource}', 'ResourceController@resourceDetails')->where('provider', '[0-9]+')->middleware('role:GA', 'role:admin', 'role:superAdmin');

    Route::get('/users', 'UserController@userList')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/users/{user}', 'UserController@userDetails')
        ->where('user', '[0-9]+')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/users/add', 'UserController@addUser')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/users/add','UserController@storeUser')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/users/{user}/edit', 'UserController@editUser')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/users/delete/{user}','UserController@deleteUser')->middleware('role:GA', 'role:admin', 'role:superAdmin');
     Route::post('/users/deletee/{user}','UserController@userDelete')->middleware('role:GA', 'role:admin', 'role:superAdmin');
     Route::post('/users/{user}/edit','UserController@updateUser')->middleware('role:GA', 'role:admin', 'role:superAdmin');

    Route::get('/temp', 'TempController@tempList')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/temp/add', 'TempController@addTemp')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/temp/add', 'TempController@storeTemp')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/temp/delete/{temp_notification}','TempController@confirmDeleteTemp')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/temp/delete/{temp_notification}','TempController@deleteTemp')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/temp/edit/{temp_notification}','TempController@editTemp')->middleware('role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/temp/edit/{temp_notification}', 'TempController@updateTemp')->middleware('role:GA', 'role:admin', 'role:superAdmin');


    Route::get('/profile','ProfileController@viewProfile')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::get('/profile/edit','ProfileController@editProfile')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');
    Route::post('/profile/edit','ProfileController@updateProfile')->middleware('role:worker', 'role:GA', 'role:admin', 'role:superAdmin');


});

Route::auth();

Route::get('/unauthorized', function () {
    $message = "You do not have permission to view this page.";
    return view('message', compact('message'));
} );