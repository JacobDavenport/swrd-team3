<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use DateTime;
use Auth;
use DB;
use App\Provider;
use App\User;

use App\Http\Requests;

class ProfileController extends Controller
{
    /**
     * Displays the users account information if the user has an id (is logged in, and exists).
     *
     * @return view "profile"
     */
    public function viewProfile()
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        if($user !== null)
        {
            $user = User::find($id);
            return view('profile.view', compact('user'));
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }

    }


 
    /**
     * Allows the user to edit their account information for only their account. 
     *
     * @return view "profile.edit"
     */
    public function editProfile()
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        if($user !== null)
        {
            return view('profile.edit', compact('user'));
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }

    /**
     * the actual database query for updating the users account information. 
     *
     * @return view "home"
     */
    public function updateProfile()
     {
         //store fields from form
        $email = Request::get('email');
        $name = Request::get('name');

        $id = Auth::user()->id;
        $user = User::find($id);
        
        $user->name = $name;
        $user->email = $email;
        $user->save();

        return view('profile.view',compact('user'));
         
 
     }

}
