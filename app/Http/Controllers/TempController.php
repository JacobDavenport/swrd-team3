<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Console\PhoneStringBuilder;
use App\TempNotification;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\FlashNotifier;

class TempController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * runs user through middleware and only allows certain roles to see certain parts of the page. 
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('role:worker', ['only' => ['index', 'welcome', 'cart']]);
        //$this->middleware('role:GA', ['only' => 'notifications']);
    }

    /**
     * creates an object of the TempNotification class and returns the list view. 
     *
     * @return view "temp"
     */
    public function tempList()
    {
        $notifications = DB::table('temp_notifications')->get();
        return view('temp.list', ['notifications' => $notifications]);
    }
    
    /**
     * returns the add temp view that will display a form to allow input to add a 
     * temp_notification to the database.
     *
     * @return view "home"
     */
    public function addTemp()
    {
        return view('temp.add');
    }

    /**
     * The actual queries for storing the data from the form on the add provider page. 
     *
     * @return message "Notification Added"
     */
    public function storeTemp()
    {
        /**if(Input::get('save'))
        {
            $notification = new TempNotification;
            $notification->name = $request->name;
            $notification->description = $request->description;
            $notification->date = $request->date;
            $notification->time = $request->time;
            $notification->location = $request->location;
            $notification->expiration_date = $request->expiration_date;
            $notification->event_contact = $request->event_contact;
            $notification->phone = $request->phone;
            $notification->save();


            flash()->success('Notification Added!');
            return Redirect::back()->withInput();
        }
        else
        {
            $notifications = DB::table('temp_notifications')->get();
            return view('temp.list', ['notifications' => $notifications]);
        }*/
        if(Input::get('save'))
        {
            //store fields from form
            $name = Request::get('name');
            $description = Request::get('description');
            $date = Request::get('date');
            if (preg_match("/^(\d{4})-(\d{2})-(\d{2})/", $date))
            {
                $real_date = $date;
            }
            else
            {
                $real_date = '2016-01-01';
            }
            $start_time = Request::get('start_time');
            $end_time = Request::get('end_time');
            if (preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $start_time) && preg_match("/(2[0-4]|[01][1-9]|10):([0-5][0-9])/", $end_time))
            {
                $time = date("g:i a", strtotime($start_time)) . ' - ' . date("g:i a", strtotime($end_time));
            }
            else
            {
                $time = 'N/A';
            }
            $location = Request::get('location');
            $date = Request::get('expiration_date');
            if (preg_match("/^(\d{4})-(\d{2})-(\d{2})/", $date))
            {
                $expiration_date = $date;
            }
            else
            {
                $expiration_date = '2016-01-01';
            }
            $event_contact = Request::get('event_contact');
            $phone = PhoneStringbuilder::addFormatting(PhoneStringBuilder::stripFormatting(Request::get('phone')));

            //add user to database with default password
            DB::table('temp_notifications')->insert([
                'name' => $name,
                'description' => $description,
                'date' => $real_date,
                'time' => $time,
                'location' => $location,
                'expiration_date' => $expiration_date,
                'event_contact' => $event_contact,
                'phone' => $phone,
            ]);

            //redirect to user list
            $notifications = DB::table('temp_notifications')->get();
            return view('temp.list', ['notifications' => $notifications]);
        }
        else
        {
            $notifications = DB::table('temp_notifications')->get();
            return view('temp.list', ['notifications' => $notifications]);
        }
    }

    /**
     * return the edit page for the provider to display a form to change the data.
     *
     * @return view provider.edit
     */
    public function editTemp($tempID)
    {
        //$role= Auth::user()->role;
        $notification = tempnotification::find($tempID);
        
        if($notification !== null)
        {
            return view('temp.edit', compact('notification'));
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }

    public function confirmDeleteTemp($tempID)
    {
        $notification = tempnotification::find($tempID);
        return view('temp.delete', compact('notification'));
    }

    public function deleteTemp($tempID)
    {
        if(Input::get('delete'))
        {
            $notification = tempnotification::find($tempID);
            $notification->delete();
            
            flash()->success('Item successfully deleted!');
            return redirect()->action('TempController@tempList');
        }

        else
        {
            return redirect()->action('TempController@tempList');
        }
    }

    /**
     * return the temp.list view after changing a user or cancelling out the action. 
     *
     * @param $TempID
     * @return view temp.list
     */
    public function updateTemp($tempID)
    {
        //check button 
        if(Input::get('save'))
        {

            $notification = tempnotification::find($tempID);
            
            //update information
            $name = Request::get('name');
            $description = Request::get('description');
            $date = Request::get('date');
            $time = Request::get('time');
            $location = Request::get('location');
            $expiration_date = Request::get('expiration_date');
            $event_contact = Request::get('event_contact');
            $phone = PhoneStringbuilder::addFormatting(PhoneStringBuilder::stripFormatting(Request::get('phone')));
            
            $notification->name = $name;
            $notification->description = $description;
            $notification->date = $date;
            $notification->time = $time;
            $notification->location = $location;
            $notification->expiration_date = $expiration_date;
            $notification->event_contact = $event_contact;
            $notification->phone = $phone;
            $notification->save();
            
            //return users list
            flash()->success('Item successfully saved! ');
            return redirect()->action('TempController@tempList');
        }

        else 
        {
            //if cancel button selected, return users list
            return redirect()->action('TempController@tempList');
        }
    }
}
