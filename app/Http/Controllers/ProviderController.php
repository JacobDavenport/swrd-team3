<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FlagComments;
use DB;
use App\Provider;
use App\User;
use Illuminate\Queue\Connectors\RedisConnector;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\FlashNotifier;

class ProviderController extends Controller
{
    /**
     * Limits the users levels that access parts of the page.
     *
     * Last updated: 18 September 2016 by Travis Rich
     *  Changes:
     *      Changed 'feed' to 'fees' in storeProvider()
     *
     * @return void
     */


    
    public function __construct()
    {
        //$this->middleware('role:worker', ['only' => ['providerList', 'providerDetails']]);
        //$this->middleware('role:GA', ['only' => ['addProvider', 'storeProvider', 'editProvider', 'updateProvider', 'archive', 'archiveProvider', 'flaggedProvider']]);
        //$this->middleware('role:admin', ['only' => []]);
        //$this->middleware('role:superAdmin', ['only' => []]);
    }

    /**
     * creates an object of the provider class and returns the list view. 
     *
     * @return view "home"
     */
    public function providerList()
    {
        $providers = Provider::where('flag_archived', 0)->get();
        return view('provider.list', compact('providers'));
    }

    /**
     * Take in the id of the provider selected and display all the information from the database.
     *
     * @param $providerID 
     * @return view "home"
     */
    public function providerDetails($providerID)
    {

        $provider = Provider::find($providerID);
        if($provider->flag_archived == 1)
        {
            $provider->flag_archived = 'yes';
        }
        else
        {
            $provider->flag_archived = 'no';
        }
        if($provider->flag_out_of_date == 1)
        {
            $provider->flag_out_of_date = 'yes';
        }
        else
        {
            $provider->flag_out_of_date = 'no';
        }

        if($provider->web=="")
        {
            $provider->web = "No website listed";
        }


        if($provider !== null)
        {
            return view('provider.details', compact('provider'));
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }

    /**
     * returns the add provider view that will display a form to allow input to add a 
     * provider to the database.
     *
     * @return view "home"
     */
    public function addProvider()
    {
        $resources = DB::table('resources')->get();
        return view('provider.add', ['resources' => $resources]);
    }

    /**
     * The actual queries for storing the data from the form on the add provider page. 
     *
     * @return message "Provider Added"
     */
    public function storeProvider(Request $request)
    {
        if(Input::get('save'))
        {
            //Store the local table data
            $provider = new Provider;
            $provider->name = $request->name;
            $provider->street = $request->street;
            $provider->city = $request->city;
            $provider->county = $request->county;
            $provider->zip = $request->zip;
            $provider->state = $request->state;
            $provider->email = $request->email;
            $provider->web = $request->web;
            $provider->contact_f_name = $request->contact_f_name;
            $provider->contact_l_name = $request->contact_l_name;
            $provider->location = $request->location;
            $provider->population = $request->population;
            $provider->office_hours = $request->office_hours;
            $provider->description = $request->description;
            $provider->intake = $request->intake;
            $provider->fees = $request->fees;

            $provider->save();

            //Store the phone numbers
            $phones = Input::get('phoneNumber');
            $notes = Input::get('phoneNote');
            if($phones)
            {
                foreach($phones as $key=>$p)
                {
                    $provider->storePhone($p, $notes[$key]);
                }
            }

            //Store the linked resources
            $resources = Input::get('resource');
            if($resources)
            {
                foreach( $resources as $r)
                {
                    $provider->storeLinkedResource($r);
                }
            }

            flash()->success('Provider Added!');
            return redirect()->action('ProviderController@providerList');
        }
        else
        {
            return redirect()->action('ProviderController@providerList');
        }
    }
    
    /**
     * return the edit page for the provider to display a form to change the data.
     *
     * @return view provider.edit
     */
    public function editProvider($providerID)
    {
        //$role= Auth::user()->role;
        $provider = Provider::find($providerID);
        $resources = DB::table('resources')->get();
        
        if($provider !== null)
        {
            return view('provider.edit', compact('provider'), ['resources' => $resources]);
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }

    /**
     * populate the search bar when clicking on the buttons on the dashboard.
     *
     * @return view provider.list
     */
    public function searchProvider($searchTerm)
    {
        $providers = Provider::where('flag_archived', 0)->get();
        return view('provider.list', compact('providers'))->with('searchTerm',$searchTerm);
    }

    /**
     * return the provider.list view after either changing a user, deleting a user, or cancelling out the action. 
     *
     * @param $providerID
     * @return view provider.list
     */
    public function updateProvider($providerID, Request $request)
    {
        //check button 
        if(Input::get('save'))
        {

            $provider = Provider::find($providerID);
     
            //update information
            $provider->name = $request->name;
            $provider->street = $request->street;
            $provider->city = $request->city;
            $provider->county = $request->county;
            $provider->zip = $request->zip;
            $provider->state = $request->state;
            $provider->email = $request->email;
            $provider->web = $request->web;
            $provider->contact_f_name = $request->contact_f_name;
            $provider->contact_l_name = $request->contact_l_name;
            $provider->location = $request->location;
            $provider->population = $request->population;
            $provider->office_hours = $request->office_hours;
            $provider->flag_archived = $request->flag_archived;
            $provider->description = $request->description;
            $provider->intake = $request->intake;
            $provider->fees = $request->fees;
            $provider->save();

            //Clear and rebuild the phone numbers
            $provider->clearPhones();

            $phones = Input::get('phoneNumber');
            $notes = Input::get('phoneNote');
            if($phones)
            {
                foreach($phones as $key=>$p)
                {
                    $provider->storePhone($p, $notes[$key]);
                }
            }

            //Clear and rebuild the linked resources
            $provider->clearLinkedResources();

            $resources = Input::get('resource');
            if($resources)
            {
                foreach( $resources as $r)
                {
                    $provider->storeLinkedResource($r);
                }
            }

            flash()->success('Item successfully saved! ');
            return redirect()->action('ProviderController@providerList');
        }
        elseif(Input::get('delete'))
        {
            //if delete button, pass ID to delete function

            $provider = Provider::find($providerID);

            $provider->flag_archived = 1;
            $provider->save();



            flash()->success('Item successfully archived! ');
            return redirect()->action('ProviderController@providerList');


        }
        else 
        {
            //if cancel button selected, return users list
            return redirect()->action('ProviderController@providerList');
        }
    }


    //Stores an out of date comment and flags provider as out of date
    public function storeOOD(Request $request)
    {
        // saves flag comment to DB with provider being flagged
        $flag_comment = new FlagComments;
        $flag_comment->provider_id = \Request::get('providerID');
        $flag_comment->comment_text = str_replace( Array("\r\n","\n","\r"), " " ,\Request::get('newComment'));
        $flag_comment->save();
        //Flags provider as out of date
        $provider = Provider::find(\Request::get('providerID'));
        $provider->flag_out_of_date = 1;
        $provider->save();

        flash()->success("Thank you for submitting a correction, we will implement it as soon as possible.");
       // return Redirect::back()->withInput();
       return redirect()->action('ProviderController@providerList');


    }

    //Adds provider to "cart" for printing
    public function addToCart($providerID)
    {



        $provider = Provider::find($providerID);
        Cache::add($providerID, $provider, 160);
        $mesaage = 'Item has been added to cart';
        flash()->success('Item successfully added to the cart! ');
        return Redirect::back()->withInput();

    }

    //archives a provider
    public function archive($providerID)
    {
        $provider = Provider::find($providerID);

        $provider->flag_archived = 1;
        $provider->save();


        flash()->success('Item successfully archived! ');
        return Redirect::back()->withInput();

    }

    //returns a list of out of date providers
    public function flaggedProvider()
    {
        $providers = Provider::where('flag_out_of_date', 1)->get();
        
       return view('provider.flagged', compact('providers'));
    }

    //creates and displays pdf from providers in cart
   public function PDF()
    {
        /*$value = Cache::get('name') . '<br>' . Cache::get('street') . ',' . Cache::get('city') . ',' . Cache::get('state') . '<br>' .
        Cache::get('description');*/
        $value = "<html><head>";
        $value .= "<meta http-equiv=\"Content-Type\" 
                 content=\"text/html; charset=iso-8859-15\" />";
        $value .= '<style type="text/css">
                
                 table, th, td, body {
                
                 font-style: normal;
                 font-weight: 300;
                 font-size: 12pt;
                 color: dimgrey;
                }
              
               </style></head><body>';
        $value .= '<h2 style=\"align-content: center\"> East Tennessee State University Department of Social Work</h2>
                
                <P>Resource provider list</P>';
        foreach(Provider::get() as $provider)
        {
            $id = ($provider->id);
            if(cache::has($id))
            {
                $providerArray[$id]= $provider;
            }
        }

        if(isset($providerArray))
        {
            foreach($providerArray as $pdfItem)
            {
                $value .= '<p><table class="table-bordered" style="width: 100%">  <tr><td><h4>' . $pdfItem->name . '</h4></td></tr>'
                    . '<tr><td><b>Description:</b> ' . $pdfItem->description . '</td></tr>'
                    . '<tr><td><b>Phone:</b>' . $pdfItem->formatPhone() . ',' . ' <b>Address: </b> ' . $pdfItem->street . ', '
                    .       $pdfItem->city . ', ' . $pdfItem->state . ' ' . $pdfItem->zip . ' : ' .  $pdfItem->county . ' county' . '</td></tr>'
                    . '<tr><td> <b>Contact Person: </b> ' . $pdfItem->contact_f_name . ' ' . $pdfItem->contact_l_name . '</td></tr>'
                    . '<tr><td> <b>Website: </b>' . $pdfItem->web . '</td></tr>'
                    . '<tr><td> <b>Hours of operation: </b>' . $pdfItem->office_hours . '</td></tr></p>';
            }
            $value .= '</table></body>';
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($value);
            return $pdf->stream();
        }
        else
        {

            flash()->error('There is nothing in your cart at this time. ');
            return Redirect::back()->withInput();
        }


    }
    //clears cart entirely
    public function clearCart()
    {
        Cache::flush();
        flash()->success('Cart has been cleared!');
        return Redirect::back()->withInput();
    }
    //removes single item from cart
    public function removeCartItem($providerID)
    {
        Cache::forget($providerID);
        return Redirect::back()->withInput();
    }

    //returns view of archived providers
     public function archiveProvider()
    {   
        
         $providers = Provider::where('flag_archived', 1)->get();

         return view('provider.archive', compact('providers'));  
       
    }

    //restores an archived provider to normal status
    public function restoreArchive($providerID)
    {
        $provider = Provider::find($providerID);

        $provider->flag_archived = 0;
        $provider->save();


        flash()->success('Item successfully restored! ');
        return Redirect::back()->withInput();

    }

    //removes out of date flag from a provider and deletes attached comments associated with flag
    public function unflag($providerID)
    {
        $provider = Provider::find($providerID);
        
        foreach($provider->flagComments as $comment)
        {
            echo $comment->delete();
        }
        
        $provider->flag_out_of_date = 0;
        $provider->save();


        flash()->success('Flag successfully taken cleared! ');
        return Redirect::back()->withInput();

    }
    public function editflaggedProvider($providerID)
    {
        //$role= Auth::user()->role;
        $provider = Provider::find($providerID);
        $resources = DB::table('resources')->get();
        
        if($provider !== null)
        {
            return view('provider.editFlagged', compact('provider'), ['resources' => $resources]);
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }

  public function updatesProvider($providerID, Request $request)
    {
        //check button 
        if(Input::get('save'))
        {

             $provider = Provider::find($providerID);
     
            //update information
            $provider->name = $request->name;
            $provider->street = $request->street;
            $provider->city = $request->city;
            $provider->county = $request->county;
            $provider->zip = $request->zip;
            $provider->state = $request->state;
            $provider->email = $request->email;
            $provider->web = $request->web;
            $provider->contact_f_name = $request->contact_f_name;
            $provider->contact_l_name = $request->contact_l_name;
            $provider->location = $request->location;
            $provider->population = $request->population;
            $provider->office_hours = $request->office_hours;
            $provider->flag_archived = $request->flag_archived;
            $provider->description = $request->description;
            $provider->intake = $request->intake;
            $provider->fees = $request->fees;
            $provider->save();
            
            $this->unflag($providerID);
            //Clear and rebuild the phone numbers
            $provider->clearPhones();

            $phones = Input::get('phoneNumber');
            $notes = Input::get('phoneNote');
            if($phones)
            {
                foreach($phones as $key=>$p)
                {
                    $provider->storePhone($p, $notes[$key]);
                }
            }

            //Clear and rebuild the linked resources
            $provider->clearLinkedResources();

            $resources = Input::get('resource');
            if($resources)
            {
                foreach( $resources as $r)
                {
                    $provider->storeLinkedResource($r);
                }
            }

            flash()->success('Item successfully saved! ');
            return redirect()->action('ProviderController@flaggedProvider');
        }
        elseif(Input::get('delete'))
        {
            //if delete button, pass ID to delete function

            $provider = Provider::find($providerID);

            $provider->flag_archived = 1;
            $provider->save();



            flash()->success('Item successfully archived! ');
            return redirect()->action('ProviderController@flaggedProvider');


        }
        else 
        {
            //if cancel button selected, return users list
            return redirect()->action('ProviderController@flaggedProvider');
        }
    }
}
