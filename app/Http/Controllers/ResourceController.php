<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Resource;
use App\ProviderResource;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\FlashNotifier;


class ResourceController extends Controller
{
    
    public function __construct()
    {
        //$this->middleware('role:GA', ['only' => ['addResource', 'storeResource', 'editResource']]);
        //$this->middleware('role:admin', ['only' => []]);
        //$this->middleware('role:superAdmin', ['only' => []]);
    }

    public function resourceList()
    {
      $resources = DB::table('resources')->get();
       return view('resource.list',['resources' => $resources]);
    }

    public function addResource()
    {

        return view('resource.add');
    }

    /**
     * Database queries for adding a resource. This is what will store the data from the form into the DB.
     *
     * @param $request
     * @return message 
     */
    public function storeResource(Request $request)
    {
        if(Input::get('save'))
        {
            $resource = new Resource;
            $resource->type = $request->type;
            $resource->subtype = $request->subtype;
            $resource->description = $request->description;
            $resource->save();

            flash()->success('Resource added!');
            return Redirect::back()->withInput();
        }
        else
        {
            return redirect()->action('ResourceController@resourceList');
        }
    }    

    public function editResource($resourceID)
    {
         $resource = resource::find($resourceID);
        return view('resource.edit', compact('resource'));
    }
     public function updateResource($resourceID, Request $request)
    {
        //check button 
        if(Input::get('save'))
        {

            $resource = Resource::find($resourceID);
     
            //update information
             $resource->type = $request->type;
             $resource->subtype = $request->subtype;
             $resource->description = $request->description;
           
             $resource->save();
            //return Resources list
            flash()->success('Item successfully saved! ');
            return redirect()->action('ResourceController@resourceList');
        }
        elseif(Input::get('delete'))
        {
              //if delete button, pass ID to delete function
            $this->resourceDelete($resourceID);
            //return Resources list
            $resource=Resource::all();



            flash()->success('Item successfully deleted! ');
            return redirect()->action('ResourceController@resourceList');


        }
        else 
        {
            //if cancel button selected, return Resources list
            return redirect()->action('ResourceController@resourceList');
        }
    }


    public function linkProvider()
    {
        $providers = DB::table('providers')->where('flag_archived', 0)->get();
        $resources = DB::table('resources')->get();
        return view('resource.link', ['resources' => $resources], ['providers' => $providers]);
    }

    public function storeLink(Request $request)
    {

        if(Input::get('save'))
        {
            $provider_resource = new ProviderResource;
            $provider_resource->provider_id = $request->providerId;
            $provider_resource->resource_id = $request->resourceId;
            $provider_resource->save();
            flash()->success('Link between provider and resource has been established.');
            return Redirect::back()->withInput();

        }
        else
        {
            return redirect('resource');
        }

    }
 

     public function deleteResource($resourceID)
    {
         $resource = resource::find($resourceID);
        return view('resource.delete', compact('resource'));
    }
    
     public function resourceDelete($resourceID)
    {
        //retrieve resource using ID
        $resource = Resource::find($resourceID);
        //delete resource
        $resource->delete();
            flash()->success('Item successfully deleted! ');
            return redirect()->action('ResourceController@resourceList');
        
    }
         public function searchResource($searchTerm)
    {
      //  $resources = Resource::where('flag_archived', 0)->get();
      return view('resource.list', compact('resources'))->with('searchTerm',$searchTerm);
       // return view('resource.list', compact('searchTerm',$searchTerm));
    }
}
