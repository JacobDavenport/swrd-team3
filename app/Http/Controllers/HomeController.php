<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\TempNotification;
use Illuminate\Http\Request;
use App\Provider;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * runs user through middleware and only allows certain roles to see certain parts of the page. 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:worker', ['only' => ['index', 'welcome', 'cart']]);
        $this->middleware('role:GA', ['only' => 'notifications']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    /**
     * Show the application dashboard on login.
     *
     * @return view "home"
     */
    public function welcome()
    {
/*        $notifications = TempNotification::all();
        foreach ($notifications as $cacheNote)
        {
            $id = $cacheNote->id;
            Cache::add('Notification' . $id, $cacheNote, 160);
        }*/
        return view('home');
    }
    public function cart()
    {
        foreach(Provider::get() as $provider)
        {
            $id = ($provider->id);
            if(cache::has($id))
            {
                $providerArray[$id]= $provider;
            }
        }
        return view('cart', compact('providerArray'));
    }

    public function removeCartItem($providerID)
    {
        Cache::forget($providerID);
        foreach(Provider::get() as $provider)
        {
            $id = ($provider->id);
            if(cache::has($id))
            {
                $providerArray[$id]= $provider;
            }
        }
        return view('cart', compact('providerArray'));
    }
    /**
     * Show the flagged providers in the notifications 
     * for when the user is logged in. 
     *
     * @return view "home"
     */
    public function notifications()
    {
        $providers = Provider::where('flag_out_of_date', '=', 1)->where('flag_archived', 0)->get();
        return view('notifications', compact('providers'));
    }
}
