<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
 use Request;
 use DateTime;
use Auth;
use DB;
use App\Provider;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class UserController extends Controller
{
    /**
     * This page should only be allowed for Superadmins, and admins (privelged GA.) 
     * No one else should be able to see it.
     *
     * @return void
     */
    public function __construct()
    {
         //$this->middleware('role:GA', ['only' => ['userList', 'userDetails','editUser']]);
         //$this->middleware('role:admin', ['only' => ['addUser']]);
         //$this->middleware('role:superAdmin', ['only' => [ ]]);
    }

    /**
     * returns a view that contains the full list of all the users.
     *
     * @return view user.list
     */
    public function userList()
    {
        $users = User::all();
        return view('user.list', compact('users'));
    }

    /**
     * return the details page for the a certain specified user.
     *
     * @param $userID
     * @return view user.edit
     */
    public function userDetails($userID)
    {
        $user = User::find($userID);
        if($user !== null)
        {
            $user = User::find($userID);
            return view('user.details', compact('user'));
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }
    
    /**
     * database queries to save the data from the add user form. 
     *
     * @return view user.list
     */
     public function storeUser()
     {
         //store fields from form
         $username = Request::get('username');
         $firstname = Request::get('firstname');
         $lastname = Request::get('lastname');
         $level = Request::get('level');
         $password = 'password';
         $created = new DateTime();    
 
         //add user to database with default password
             DB::table('users')->insert([
             'name' => $firstname ." ". $lastname,
             'email' => $username,
             'password' => '$2y$10$wOzsEp8QQNUc/lru8xZyne7YL9M9TzYAm.gvRRGqy9xL7FuC.WFL2',
 		    'role' => $level,
             'created_at' => $created,
              'updated_at' => $created
              ]);
 
         //redirect to user list 
         $users = User::all();
         return view('user.list', compact('users'));
         
 
     }

     /**
     * returns the view for the add user form.
     *
     * @return view user.add
     */
    public function addUser()
    {
        return view('user.add');
    }

    /**
     * displays the view to edit an existing user.
     *
     * @return view user.edit
     */
    public function editUser($userID)
    {
        $role= Auth::user()->role;
        $user = User::find($userID);
        
        if($user !== null && $user->role > $role)
        {
                return view('user.edit', compact('user'));
        }
        else
        {
             $message = "404 - Page not found"; 
             return view('message', compact('message'));
             //abort(404, 'Page not found');
        }
    }

    /**
     * return the user.list view after either changing a user, deleting a user, or cancelling out the action. 
     *
     * @param $userID
     * @return view user.list
     */
    public function updateUser($userID)
    {
        //check button
        if(Input::get('save'))
        {
            //store fields from form
            $name = Request::get('name');
            $email = Request::get('email');
            $level = Request::get('level');
            $user = User::find($userID);
     
            //update information
            $user->email = $email;
            $user->name = $name;
            $user->role = $level;
            $user->save();

            //return users list
            $users=User::all();
             return view('user.list', compact('users'));

        }
        elseif(Input::get('delete'))
        {
            //if delete button, pass ID to delete function
            $this->userDelete($userID);
            //return users list
            $users=User::all();
             return view('user.list', compact('users'));
        }
        else 
        {
            //if cancel button selected, return users list
            $users = User::all();
            return view('user.list', compact('users'));
        }
      
       
    }

    /**
     * If the delete is selected from the edit function, this function will be called to remove the user.
     *
     * @param $userID
     * @return void
     */
    public function userDelete($userID)
    {
         $role= Auth::user()->role;
    
        //retrieve user using ID
        $user = User::find($userID);
        //delete user
     $user->delete();
    
        return redirect()->action('UserController@userList');
    }

    public function deleteUser($userID)
    {
         //$role= Auth::user()->role;
    
        //retrieve user using ID
        $user = User::find($userID);
        //delete user
    // $user->delete();
     return view('user.delete', compact('user'));
        
    }
                
}
