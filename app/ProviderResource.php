<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProviderResource extends Model
{
    protected $table = 'provider_resource';

    public function providers()
    {
        return $this->belongsToMany('App\Provider');
    }

    public function resources()
    {
        return $this->belongsToMany('App\Resource');
    }
    public $timestamps = false;
}