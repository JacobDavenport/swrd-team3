<?php



if($provider->flag_out_of_date)
{
    echo "<td> Yes </td>";
}
else
{
    echo "<td> No </td>";
}
    echo "<td> $provider->name </td>";

    $formattedPhone = $provider->formatPhone();

    echo "<td> $formattedPhone </td>";
    echo "<td> $provider->description </td>";
    //Note: By coincidence, all the descriptions are short right now,
    //but there will probably be long in the future
    echo "<td> ";
    foreach($provider->resources as $resource)
    {
        echo $resource->type;
        echo " - ";
        echo $resource->subtype;
        echo "</br>";
    }
        echo " </td>";
        echo "<td> $provider->county </td>";
        echo "<td> <a href='/providers/$provider->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> View </a>";
    if (Auth::user()->hasRole('superAdmin'))
    {
        echo " <button onclick=restoreArchived($provider->id) class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Restore </button> </td>";
    }


