<?php
/**
 * Created by PhpStorm.
 * User: travis
 * Date: 11/10/16
 * Time: 9:07 PM
 */

namespace App\Console;


class PhoneStringBuilder
{
    //Strip everything that isn't a digit in 0-9; right now, it can't recognize numbers with extensions
    public static function stripFormatting($rawPhone)
    {
       return preg_replace("/[^0-9]/", "", $rawPhone);
    }

    public static function addFormatting($rawPhone)
    {
        $phoneReadable = "";

        switch(strlen($rawPhone))
        {
            case 11:
                //This number will eventually be formatted as #(###)###-####
                $phoneReadable = $phoneReadable . substr($rawPhone, 0, 1); //Push the first digit into the readable string
                $rawPhone = substr($rawPhone, 1); //Strip out the first digit from the raw version
            //fall through
            case 10:
                //Put the area code in parenthesis
                $phoneReadable = $phoneReadable . " (" . substr($rawPhone, 0, 3) . ") ";
                $rawPhone = substr($rawPhone, 3); //Strip out the area code from the raw version
            //fall through
            case 7:
                //Put a dash in the last 7 digits
                $phoneReadable = $phoneReadable . substr($rawPhone, 0, 3) . '-' . substr($rawPhone, 3);
                break;
            default:
                //The number was of an unrecognized length, so present it exactly as stored.
                $phoneReadable = $rawPhone;
        }

        return $phoneReadable;
    }
}