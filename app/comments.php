<?php

        $commentObject = $provider->FlagComments;
        $exploded = explode('},{', $commentObject);

        //explaode each array into more arays delimited by colon. This is simgling out the comments.
        $holderarray = [];// holds comments once exploding is complete
        foreach ($exploded as $newArrayPieces) {
            //further explodes array contaiing user comments
            $newArrayPieces2 = explode(':', $newArrayPieces);

            //saves only the comment portion of the single flag entry
            $newArrayPieces3 = end($newArrayPieces2);

            //Pushes the comment to the end of the array holding all comments from users about the out of date content
            array_push($holderarray, $newArrayPieces3);
        }
        //holderarray holds only the comments left by users now.
        if ($provider->flag_out_of_date == 1) {
            echo "<table class='table table-hover' style='border: 1px solid #ddd;'>";
            echo "<th> Out of date commments from users </th>";
            //remove brackets from end of array
            $arraySize = count($holderarray) - 1;
            $string = $holderarray[$arraySize];
            $holderarray[$arraySize] = substr($string, 0, -2);
            //print each comment to a table

            //take all comments and explode into array delimited by bracket and comma
            $commentObject = $provider->FlagComments;

            foreach($holderarray as $comment)
            {
                echo "<tr>";
                echo "<td>";
                $noTags = htmlspecialchars($comment);
                echo $noTags;
                echo "</td>";
                echo "</tr>";
            }

            echo "</table>";
        }//end if provider is out of date
        echo " <form action='/providers/edit/$provider->id method='>";
        echo "<input type='submit' value='Edit'>";
        echo "</form>";


