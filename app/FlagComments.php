<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlagComments extends Model
{
    /**
     * Run the migrations.
     *
     * Last updated: by Joe Schaum
     *  Changes:
     *      -Initial version
     *
     * @return void
     */
    protected $table = 'flag_comments';

    public function provider()
    {
        return $this->belongsTo('App\Provider', provider_id, comment_text);
    }

    public $timestamps = false;
}