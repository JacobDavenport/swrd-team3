<?php
/**
 * Last updated: 11 October 2016 by Travis Rich
 *  Changes:
 *      -initial Version
 */

namespace App;

use App\Console\PhoneStringBuilder;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table = 'phones';

    public function provider()
    {
        return $this->belongsTo('App\Provider', 'provider_id');
    }

    //Strip everything that isn't a digit in 0-9; right now, it can't recognize numbers with extensions
    public function setPhone($rawPhone)
    {
        $this->setAttribute('phone',PhoneStringBuilder::stripFormatting($rawPhone));
    }

    //Since the phone number is stored as digits only in the database,
    //we need to format it for presentation
    public function getFormattedPhone($withNote = false)
    {
        $phoneReadable = PhoneStringBuilder::addFormatting($this->getAttribute('phone'));

        //If the caller asked for the note and there is a note, include it as well
        if ($withNote && !empty($this->getAttribute('note')))
        {
            $phoneReadable = " " . $this->getAttribute('note') . ":" . $phoneReadable;
        }

        return $phoneReadable;
    }

    public $timestamps = false;
}