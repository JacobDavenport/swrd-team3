<?php

/**
 * Last updated: 11 October 2016 by Travis Rich
 *  Changes:
 *      -Bound new phone table
 *      -storePhone() now stores a new phone number
 *      -formatPhone() now returns a formatted list of phone numbers
 *  (30 SEP 2016)
 *      -Bound new flag_comments talble
 *  (18 SEP 2016):
 *      -Added Phone formatting functions
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    protected $table = 'providers';

    public function resources()
    {
        return $this->belongsToMany('App\Resource', 'provider_resource', 'provider_id', 'resource_id');
    }

    public function phones()
    {
        return $this->hasMany('App\Phone');
    }

    public function flagComments()
    {
        return $this->hasMany('App\FlagComments');
    }

    public function providerResources()
    {
        return $this->hasMany('App/ProviderResources');
    }

    //Store a collection of phone numbers with comments, delimited by ";"
    public function storePhone($number, $note)
    {
        $phone = new Phone();
        $phone->provider_id = $this->getAttribute('id');
        $phone->setPhone($number);
        $phone->note = $note;
        $this->phones()->save($phone);
    }

    public function clearPhones()
    {
        Phone::where('provider_id', '=', $this->getAttribute('id'))->delete();
    }

    public function storeLinkedResource($resourceID)
    {
        $provider_resource = new ProviderResource;
        $provider_resource->provider_id = $this->getAttribute('id');
        $provider_resource->resource_id = $resourceID;
        $provider_resource->save();
    }

    public function clearLinkedResources()
    {
        ProviderResource::where('provider_id', '=', $this->getAttribute('id'))->delete();
    }

    //format and print the phone numbers associated with this provider as a single string
    public function formatPhone($isWithNotes = true)
    {
        $clnPhoneNumbers = array();

        $clnPhones = Phone::where('provider_id', '=', $this->getAttribute('id'))->get();

        /* @var $phone \App\Phone */
        foreach ( $clnPhones as $phone)//($this->phones() as $phone)
        {
            $clnPhoneNumbers[] = $phone->getFormattedPhone($isWithNotes);
        }

        return join("; ", $clnPhoneNumbers);
    }
}
