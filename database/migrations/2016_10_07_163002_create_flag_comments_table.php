<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlagCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Last updated: 30 September 2016 by Travis Rich
     *  Changes:
     *      -Initial Version
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flag_comments', function (Blueprint $table) {
            $table->increments('id');         //id INTEGER,
            $table->integer('provider_id');   //name VARCHAR(100)
            $table->text('comment_text');     //comment_text TEXT
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flag_comments');
    }
}
