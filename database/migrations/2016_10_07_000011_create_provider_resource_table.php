<?php
/**
 * Last updated: 22 OCT 2016 by Travis Rich
 *  Changes:
 *      -Moved seed data to new ProviderResource seeder
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provider_resource', function (Blueprint $table) {
            $table->increments('id');           //id INTEGER,
            $table->integer('provider_id');
            $table->integer('resource_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('provider_resource');
    }
}
