<?php
/**
 * Last updated: 20 OCT 2016 by Travis Rich
 *  Changes:
 *      -Moved seed data to new Resource seeder
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');           //id INTEGER,
            $table->string('type', 100);        //type VARCHAR(100)
            $table->string('subtype', 100);     //subtype VARCHAR(100)
            $table->string('description', 200); //description VARCHAR(10)
            $table->integer('hits');            //count the number of hits for the table
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('resources');

    }
}
