<?php
/**
 * Last updated: 11 OCT 2016 by Travis Rich
 *  Changes:
 *      -Initial Version
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phones', function (Blueprint $table) {
            $table->increments('id');           //id INTEGER,
            $table->integer('provider_id');
            $table->string('phone', 11);         //phone VARCHAR(11)
            $table->string('note', 255)->nullable();        //email VARCHAR(255)

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phones');
    }
}