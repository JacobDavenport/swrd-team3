<?php
/**
 * Last updated: 11 OCT 2016 by Travis Rich
 *  Changes:
 *      -Corrected down() to enable proper migration refresh
 * (07 OCT 2016)
 *      -Moved seed data to new Roles seeder
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
