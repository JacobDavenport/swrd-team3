<?php
/**
 * CReated by Joe Schaum on Oct 23 2016
 *
 *
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('temp_notifications', function (Blueprint $table) {
            $table->increments('id');           //id INTEGER,
            $table->string('name', 100);        //type VARCHAR(100)
            $table->string('description', 200);     //subtype VARCHAR(200)
            $table->date('date'); //description VARCHAR(100)
            $table->string('time', 200);            //time of event
            $table->string('location', 200);        //location of event
            $table->date('expiration_date');        //notification expires on
            $table->string('event_contact', 150);   //person in charge of event
            $table->string('phone', 20);            //contact phone
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('temp_notifications');

    }
}
