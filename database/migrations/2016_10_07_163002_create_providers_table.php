<?php
/**
 * Last updated: 22 OCT 2016 by Travis Rich
 *  Changes:
 *      -Moved seed data to new Provider seeder
 *  (30 SEP 2016):
 *      -Moved flag_out_of_date_comment to new flag_comments table, updated seed script
 *  (18 SEP 2016):
 *      -Changed phone from varchar(8) to varchar(11)
 *      -Changed email from varchar(40) to varchar(255)
 *      -Changed description and flag_out_of_date_comment from BLOB to TEXT
 *      -Corrected 'feed' to 'fees'
 */
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');           //id INTEGER,
            $table->string('name', 100);        //name VARCHAR(100)
            $table->string('street', 70);       //street VARCHAR(70)
            $table->string('city', 40);         //city VARCHAR(40)
            $table->string('county', 40);       //county VARCHAR(40)
            $table->string('zip', 5);           //zip VARCHAR(5)
            $table->string('state', 2);         //state VARCHAR(2)
            $table->string('phone', 11);         //phone VARCHAR(11)
            $table->string('email', 255);        //email VARCHAR(40)
            $table->string('web', 100);         //web VARCHAR(100)
            $table->string('contact_f_name', 30);      //contact_f_name VARCHAR(30)
            $table->string('contact_l_name', 30);      //contact_l_name VARCHAR(30)
            $table->string('location', 40);     //location_constraint VARCHAR(40)
            $table->string('population', 250);  //population_constraint VARCHAR(100)
            $table->string('office_hours', 250); //office_hours VARCHAR(12)
            $table->boolean('flag_archived');               //flag BOOLEAN
            $table->boolean('flag_out_of_date');            //flag BOOLEAN
            $table->text('description');      //description TEXT
            $table->string('intake', 50);       //intake VARCHAR(50)??
            $table->decimal('fees');            //fees DECIMAL
            $table->timestamps();
            $table->integer('hits');            //hit counter 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('providers');
    }
}
