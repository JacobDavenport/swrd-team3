<?php
/**
 * Last updated: 11 OCT 2016 by Travis Rich
 *  Changes:
 *      -Changed data to actual sample records
 * (07 OCT 2016)
 *      -Initial version; data transferred from Provider migration
 */
use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('providers')->insert([
            //id = 1
            'name' => 'Carter County Adult Education program',
            'street' => '211 N. Church St',
            'city' => 'Mountain City',
            'county' => 'Carter',
            'zip' => '37863',
            'state' => 'TN',
            'email' => 'crussom@k12.net',
            'web' => '',
            'contact_f_name' => 'Carol',
            'contact_l_name' => 'Russum',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Offers GED preparation and adult literacy courses',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 2
            'name' => 'Greene County Adult Education program',
            'street' => '318 Tusculum Blvd',
            'city' => 'Greeneville',
            'county' => 'Greene',
            'zip' => '37745',
            'state' => 'TN',
            'email' => 'gassk@gcschools.net',
            'web' => '',
            'contact_f_name' => 'Kim',
            'contact_l_name' => 'Gass',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Offers GED preparation and adult literacy courses',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 3
            'name' => 'Northeast Tennessee Career Center Elizabethton',
            'street' => '386 Highway 91',
            'city' => 'Elizabethton',
            'county' => '',
            'zip' => '',
            'state' => 'TN',
            'email' => '',
            'web' => '',
            'contact_f_name' => '',
            'contact_l_name' => '',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Provides job seekers with job assistance career information. Each center offers internet access, labor market information, employment workshops, job placement, recruitment, and training referrals.',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 4
            'name' => 'Northeast Tennessee Career Center Johnson City',
            'street' => '2515 Wesley Street',
            'city' => 'Johnson City',
            'county' => '',
            'zip' => '37601',
            'state' => 'TN',
            'email' => '',
            'web' => '',
            'contact_f_name' => '',
            'contact_l_name' => '',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Provides job seekers with job assistance career information. Each center offers internet access, labor market information, employment workshops, job placement, recruitment, and training referrals.',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 5
            'name' => 'State of TN Employment Security Department of Employment Services',
            'street' => '2515 Wesley Street',
            'city' => 'Johnson City',
            'county' => '',
            'zip' => '37601',
            'state' => 'TN',
            'email' => '',
            'web' => '',
            'contact_f_name' => '',
            'contact_l_name' => '',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'The Employment Security Division provides a safety net for workers who have lost their jobs through no fault of their own.',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 6
            'name' => 'Charles O. Gordon Sr. Center of Hope (Salvation Army)',
            'street' => '200 Ashe St.',
            'city' => 'Johnson City',
            'county' => '',
            'zip' => '37604',
            'state' => 'TN',
            'email' => '',
            'web' => '',
            'contact_f_name' => '',
            'contact_l_name' => '',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Emergency shelter for transient clients, transitional housing for longer term states, and VA per diem program for veterans. The shelter also offers individual case management, life skills courses and budgeting workshops for longer stay residents. Hot lunch and dinner are also provided for anyone in need of a meal.',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 7
            'name' => 'Hope Haven',
            'street' => '670 Dale St.',
            'city' => 'Kingsport',
            'county' => '',
            'zip' => '37660',
            'state' => 'TN',
            'email' => '',
            'web' => '',
            'contact_f_name' => '',
            'contact_l_name' => '',
            'location' => '',
            'population' => '',
            'office_hours' => '',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Provides emergency assistance including shelter, food, and clothing for men, women, and children.',
            'intake' => '',
            'fees' => '',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        //////////////////////////////////////
        //  Bogus test data from here on    //
        //////////////////////////////////////

        DB::table('providers')->insert([
            //id = 8
            'name' => 'St. Mary\'s',
            'street' => '2300 East Stone Dr',
            'city' => 'Kingsport',
            'county' => 'Sullivan',
            'zip' => '75660',
            'state' => 'TN',
            'email' => 'contact@stymaryskingsport.com',
            'web' => 'www.stymaryskingsport.com',
            'location' => 'Kingsport',
            'population' => 'Any',
            'office_hours' => 'Mon - Fri 10 am - 6 pm',
            'flag_archived' => 0,
            'flag_out_of_date' => 1,
            'description' => 'St. Mary\'s provides shelter, meals, and hospitality to victims of sexual assault.',
            'intake' => 'walk in',
            'fees' => 'none for walk in. Meals are $3.',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 9
            'name' => 'Kevin\'s Clothing',
            'street' => '1816 Kingsport Hwy',
            'city' => 'Bristol',
            'county' => 'Sullivan',
            'zip' => '37999',
            'state' => 'TN',
            'email' => 'help@kevinskingsport.com',
            'web' => 'www.kevinskingsport.com',
            'location' => 'Sullivan County',
            'population' => 'Low or fixed income new clothing line',
            'office_hours' => 'Monday, Wednesday 9AM - 3PM',
            'flag_archived' => 0,
            'flag_out_of_date' => 1,
            'description' => 'Kevin\'s Clothing is low cost closet for fixed income families.',
            'intake' => 'walk in',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
        DB::table('providers')->insert([
            //id = 10
            'name' => 'Haven of Mercy Rescue Mission',
            'street' => '23 North Roan St',
            'city' => 'Johnson City',
            'county' => 'Washington',
            'zip' => '75601',
            'state' => 'TN',
            'email' => 'help@HavenOfMercyJC.com',
            'web' => 'http://www.HaveOfMercyJC.com',
            'location' => 'Washington County',
            'population' => 'Residents of Washington Co.',
            'office_hours' => '24/7',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Homeless shelter and services for Johnson City',
            'intake' => 'walk in',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
        DB::table('providers')->insert([
            //id = 11
            'name' => 'Interfaith Hospitality',
            'street' => '210 W Fairview Ave',
            'city' => 'Johnson City',
            'county' => 'Washington',
            'zip' => '75601',
            'state' => 'TN',
            'email' => 'help@ifhjc.com',
            'web' => 'http://www.ifhjc.com',
            'location' => 'Washington County',
            'population' => 'Homleless families with children including domestic violence situation when children are in the family. NO INDIVIDUALS. MARRIED COUPLES WITHOUT CHILDREN ARE ACCEPTED BUT WILL BE PLACED BEHIND FAMILIES WITH CHILDREN ON THE WAITING LIST.',
            'office_hours' => '24/7',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Shelter and food for homeless families with children ONLY! Case management for families to work on goals and objectives while waiting to find housing Provision of all needs while in the program and assistance in moving into housing.',
            'intake' => 'walk in',
            'fees' => 'free',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
        DB::table('providers')->insert([
            //id = 12
            'name' => 'Salvation Army',
            'street' => '505 Dale Street',
            'city' => 'Kingsport',
            'county' => 'Gray',
            'zip' => '37660',
            'state' => 'TN',
            'email' => 'help@sa.org',
            'web' => 'http://www.sa.org',
            'location' => 'none',
            'population' => '',
            'office_hours' => 'Shelter is open 24/7',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'The Salvation Army of Greater Kingsport offers a long-term program to the needy who want to help themselves This is called the Solid Rock program Solid Rock allows the individual to stay in the shelter at no cost, receiving three meals a day.',
            'intake' => 'walk in',
            'fees' => 'free',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
        DB::table('providers')->insert([
            //id = 13
            'name' => 'Estate auction in remembrance of Steve Gully.',
            'street' => '230 Seabrook Ln.',
            'city' => 'Johnson City',
            'county' => 'Washington',
            'zip' => '75601',
            'state' => 'TN',
            'email' => 'janice@realtorsJC.com',
            'web' => 'http://www.realtorsJC.com',
            'location' => 'Washington county residents only.',
            'population' => 'none',
            'office_hours' => '24/7',
            'flag_archived' => 1,
            'flag_out_of_date' => 0,
            'description' => 'Homeless shelter and services for Johnson City',
            'intake' => 'register online',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
        DB::table('providers')->insert([
            //id = 14
            'name' => 'South side tax assistance',
            'street' => '1998 wakeshore drive',
            'city' => 'Johnson City',
            'county' => 'Washington',
            'zip' => '75601',
            'state' => 'TN',
            'email' => 'taxhelpnow@taxhelp.com',
            'web' => 'Taxhelpnow.org',
            'location' => 'Low income Washington county residents only.',
            'population' => 'none',
            'office_hours' => 'Mon- Fri 8am-4:30pm',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Offers free tax advice and return filing for low income residents of Washington county',
            'intake' => 'Call for appointment',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 15
            'name' => 'East Tennessee Coalition for Change',
            'street' => '9987 executive drive',
            'city' => 'Johnson City',
            'county' => 'Washington',
            'zip' => '75601',
            'state' => 'TN',
            'email' => 'ETCC@change.org',
            'web' => 'ETCC.org',
            'location' => 'Washington County',
            'population' => 'none',
            'office_hours' => 'Mon- Fri 8am-5:30pm',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Organizes food drives, gods pantry, clothing drives, and resources for low income families' ,
            'intake' => 'walk in or visit website',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);


        DB::table('providers')->insert([
            //id = 16
            'name' => 'Kings Center',
            'street' => '143 hopsin blvd',
            'city' => 'Grey',
            'county' => 'Grey',
            'zip' => '55487',
            'state' => 'TN',
            'email' => 'KC@help.org',
            'web' => 'kingcenter.com',
            'location' => 'Grey County',
            'population' => 'Low income minor diversion program to help reduce gang activity in the youth of TN',
            'office_hours' => '24/7 live in center',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'Group home for troubled teens and children' ,
            'intake' => 'Court order',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

        DB::table('providers')->insert([
            //id = 17
            'name' => 'Social services TN',
            'street' => '65 Bright way',
            'city' => 'Johnson City',
            'county' => 'Washington',
            'zip' => '75601',
            'state' => 'TN',
            'email' => 'SS@tn.gov',
            'web' => 'SocialServices.tn.gov',
            'location' => 'Washington County',
            'population' => 'Residents of TN',
            'office_hours' => 'Mon- Fri 8am-5:30pm',
            'flag_archived' => 0,
            'flag_out_of_date' => 0,
            'description' => 'General social services and resources for TN residents' ,
            'intake' => 'Walk in, call for apointment, or visit website.',
            'fees' => 'none',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
    }
}
