<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
/**
 * Created by PhpStorm.
 * User: Joe Schaum
 * Date: 10/23/2016
 * Time: 10:18 PM
 */
class TempNotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('temp_notifications')->insert([
            'date' => '2016-10-31',
            'description' => 'Tony is turning 30, lets celebrate with BBQ and fun',
            'name' => "Tony's BBQ party",
            'time' => '7-??',
            'location' => '129 Bolero way, Johnson City, TN',
            'expiration_date' => '2016-11-01',
            'event_contact' => 'Jamie Bowers',
            'phone' => '(423)-866-5877',
        ]);

        DB::table('temp_notifications')->insert([
            'date' => '2016-11-25',
            'description' => 'Saint Johns church is hosting a free banquet for all Washington county residents',
            'name' => 'Community Feast',
            'time' => '2pm-6pm',
            'location' => '240 Churchview Road, Johnson City, TN',
            'expiration_date' => '2016-11-27',
            'event_contact' => 'John Bishop',
            'phone' => '(423)-834-5908',
        ]);

    }
}