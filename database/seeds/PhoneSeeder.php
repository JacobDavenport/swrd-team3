<?php
/**
 * Last updated: 22 OCT 2016 by Travis Rich\
 *  Changes:
 *      -Folded in test data from other branch
 *  (11 OCT 2016):
 *      -Initial version
 */
use Illuminate\Database\Seeder;

class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('phones')->insert([
            'provider_id' => '1',
            'phone' => '4235181212',
            'note' => 'POC'
        ]);

        DB::table('phones')->insert([
            'provider_id' => '2',
            'phone' => '4236382512',
            'note' => 'POC'
        ]);

        DB::table('phones')->insert([
            'provider_id' => '3',
            'phone' => '4235477500',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '4',
            'phone' => '4236100222',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '5',
            'phone' => '4239896600',
            'note' => 'Bristol'
        ]);

        DB::table('phones')->insert([
            'provider_id' => '5',
            'phone' => '4232241800',
            'note' => 'Kingsport'
        ]);

        DB::table('phones')->insert([
            'provider_id' => '6',
            'phone' => '4239268901',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '6',
            'phone' => '4239262102',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '7',
            'phone' => '4232466012',
            'note' => ''
        ]);

        //////////////////////////////////////
        //  Bogus test data from here on    //
        //////////////////////////////////////

        DB::table('phones')->insert([
            'provider_id' => '8',
            'phone' => '4238618312',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '9',
            'phone' => '4235555555',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '10',
            'phone' => '4238334582',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '11',
            'phone' => '4238334582',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '12',
            'phone' => '4238338520',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '13',
            'phone' => '4237785461',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '14',
            'phone' => '4238899754',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '15',
            'phone' => '4235471132',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '16',
            'phone' => '4246998847',
            'note' => ''
        ]);

        DB::table('phones')->insert([
            'provider_id' => '17',
            'phone' => '8005789568',
            'note' => ''
        ]);

    }
}
