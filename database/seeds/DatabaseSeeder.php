<?php
/**
 * Last updated: 11 OCT 2016 by Travis Rich
 *  Changes:
 *      -added phone seeder
 *  (07 OCT 2016)
 *      -Initial version
 */

    use Illuminate\Database\Seeder;
    use database\seeds;


class DatabaseSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $this->call(RoleSeeder::class);
            $this->call(UserSeeder::class);
            $this->call(ProviderSeeder::class);
            $this->call(ResourceSeeder::class);
            $this->call(ProviderResourceSeeder::class);
            $this->call(PhoneSeeder::class);
            $this->call(TempNotificationSeeder::class);
        }

}
