<?php
/**
 * Last updated: 07 OCT 2016 by Travis Rich
 *  Changes:
 *      -Initial version; data transferred from Roles migration
 */
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'superAdmin'
        ]);

        DB::table('roles')->insert([
            'name' => 'admin'
        ]);

        DB::table('roles')->insert([
            'name' => 'GA'
        ]);

        DB::table('roles')->insert([
            'name' => 'worker'
        ]);

        DB::table('roles')->insert([
            'name' => 'waiting'
        ]);
    }
}
