<?php
/**
 * Last updated: 22 OCT 2016 by Travis Rich
 *  Changes:
 *      -Folded in data from other branch
 * (11 OCT 2016)
 *      -Changed data to actual sample records
 * (07 OCT 2016)
 *      -Initial version; data transferred from Provider_Resource migration
 */
use Illuminate\Database\Seeder;

class ProviderResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provider_resource')->insert([
            'provider_id' => '1',
            'resource_id' => '1',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '2',
            'resource_id' => '1',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '3',
            'resource_id' => '2',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '4',
            'resource_id' => '2',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '5',
            'resource_id' => '2',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '6',
            'resource_id' => '3',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '6',
            'resource_id' => '4',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '7',
            'resource_id' => '3',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '7',
            'resource_id' => '4',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '7',
            'resource_id' => '5',
        ]);

        //////////////////////////////////////
        //  Bogus test data from here on    //
        //////////////////////////////////////

        DB::table('provider_resource')->insert([
            'provider_id' => '8',
            'resource_id' => '6',
        ]);

        DB::table('provider_resource')->insert([
            'provider_id' => '9',
            'resource_id' => '8',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '10',
            'resource_id' => '6',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '10',
            'resource_id' => '9',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '11',
            'resource_id' => '10',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '12',
            'resource_id' => '6',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '12',
            'resource_id' => '11',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '13',
            'resource_id' => '12',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '12',
            'resource_id' => '19',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '14',
            'resource_id' => '16',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '15',
            'resource_id' => '17',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '15',
            'resource_id' => '18',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '16',
            'resource_id' => '15',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '16',
            'resource_id' => '17',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '16',
            'resource_id' => '6',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '6',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '8',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '9',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '11',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '13',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '14',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '15',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '17',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '18',
        ]);
        DB::table('provider_resource')->insert([
            'provider_id' => '17',
            'resource_id' => '19',
        ]);
    }
}
