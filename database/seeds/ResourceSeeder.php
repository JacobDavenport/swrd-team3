<?php
/**
 * Last updated: 11 OCT 2016 by Travis Rich
 *  Changes:
 *      -Changed data to actual sample records
 * (07 OCT 2016)
 *      -Initial version; data transferred from resource migration
 */
use Illuminate\Database\Seeder;

class ResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resources')->insert([
            //id = 1
            'type' => 'Education',
            'subtype' => 'Adult Education Program',
            'description' => 'GED and adult literacy courses'
        ]);

        DB::table('resources')->insert([
            //id = 2
            'type' => 'Job',
            'subtype' => 'Career Center',
            'description' => 'Provides job seekers with job assistance career information'
        ]);

        DB::table('resources')->insert([
            //id = 3
            'type' => 'Housing',
            'subtype' => 'Shelter',
            'description' => 'Provides temporary shelter for individuals in need'
        ]);

        DB::table('resources')->insert([
            //id = 4
            'type' => 'Food',
            'subtype' => 'Meals',
            'description' => 'This provider provides prepared meals for applicants'
        ]);

        DB::table('resources')->insert([
            //id = 5
            'type' => 'Clothing',
            'subtype' => 'Clothing',
            'description' => 'This provider provides clothing for individuals in need'
        ]);

        //////////////////////////////////////
        //  Bogus test data from here on    //
        //////////////////////////////////////

        DB::table('resources')->insert([
            //id = 6
            'type' => 'Food',
            'subtype' => 'Meals',
            'description' => 'A food bank that offers meals to individuals who are homeless or receive low or fixed incomes.'
        ]);
        DB::table('resources')->insert([
            //id = 7
            'type' => 'Food',
            'subtype' => 'Pantry',
            'description' => 'Programs that acquire food products through donations, canned food drives, food bank programs or direct purchase and distribute the food to people.'
        ]);
        DB::table('resources')->insert([
            //id = 8
            'type' => 'Clothing',
            'subtype' => 'Clothing Closet',
            'description' => 'Free or cheap jackets, other outerwear.'
        ]);
        DB::table('resources')->insert([
            //id = 9
            'type' => 'Housing',
            'subtype' => 'Emergency Shelter',
            'description' => 'Programs that provide a temporary place to stay.'
        ]);
        DB::table('resources')->insert([
            //id = 10
            'type' => 'Housing',
            'subtype' => 'Mission',
            'description' => 'Programs with religious affiliations that provide temporary shelter for homeless people.'
        ]);
        DB::table('resources')->insert([
            //id = 11
            'type' => 'Housing',
            'subtype' => 'Moving Assistance',
            'description' => 'Programs that provide assistance for people who are moving from one residence to another or who are moving their homes from one location to another.'
        ]);
        DB::table('resources')->insert([
            //id = 12
            'type' => 'Housing',
            'subtype' => 'Real Estate Auctions',
            'description' => 'Programs that provide assistance for people who are moving from one residence to another or who are moving their homes from one location to another.'
        ]);
        DB::table('resources')->insert([
            //id = 13
            'type' => 'Utility Assistance',
            'subtype' => 'Heating bill assistance',
            'description' => 'Programs that provide winter time heating assistance.'
        ]);
        DB::table('resources')->insert([
            //id = 14
            'type' => 'Rent Assistance',
            'subtype' => 'Section 8 paperwork assistance',
            'description' => 'Programs that provide assistance in filing for section 8 rent assistance.'
        ]);
        DB::table('resources')->insert([
            //id = 15
            'type' => 'Legal Services',
            'subtype' => 'Legal council and advice',
            'description' => 'Programs that provide free legal council and advice'
        ]);
        DB::table('resources')->insert([
            //id = 16
            'type' => 'Tax Assistance',
            'subtype' => 'Tax advice and return preparation',
            'description' => 'Programs that provide tax advice and return filing assistance.'
        ]);
        DB::table('resources')->insert([
            //id = 17
            'type' => 'Transportation',
            'subtype' => 'Rideshare',
            'description' => 'Programs that provide a carpooling for low income families.'
        ]);
        DB::table('resources')->insert([
            //id = 18
            'type' => 'Medical',
            'subtype' => 'Community Clinic',
            'description' => 'Programs that provide healthcare for the community.'
        ]);
        DB::table('resources')->insert([
            //id = 19
            'type' => 'Job Assistance',
            'subtype' => 'Employment training',
            'description' => 'Programs that provide employability training.'
        ]);
    }
}
