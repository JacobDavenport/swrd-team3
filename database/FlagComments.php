<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlagComments extends Model
{
    /**
     * Run the migrations.
     *
     * Last updated: 30 September 2016 by Travis Rich
     *  Changes:
     *      -Initial version
     *
     * @return void
     */
    protected $table = 'flag_comments';

    public function provider()
    {
        return $this->belongsTo('App\Provider',id, provider_id, comment_text);
    }
    public $timestamps = false;
}
