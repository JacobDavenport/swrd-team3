@extends('layouts.app')

@section('title')
Users List
@stop

@section('content')

<?php

   /* function printUser($user)
    {
        echo "Name: $user->name";
        echo "</br>Email: $user->email";
        echo "</br>Permissions Level: ";
        echo $user->getRoleFormatted();
        echo "</br><a href='/users/$user->id'>Details</a>";        
    }
    */
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
    <!-- page content -->
          <div class="">
          <div class="clearfix"></div>
             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title">
                    <h2>User List</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>


                              <th style="width: 20%">Role</th>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Permissions</th>
                              <th>Options</th>
                            </tr>
                          </thead>
                         <tbody> 
                            <!-- Query the User database table -->
                             <?php 
                                function printUser($user)
                                {

                                   // echo "<td> #</td>";


                                    echo "<td>";
                                    echo $user->getRoleFormatted();
                                    echo "</td>";
                                    echo "<td> $user->name </td>";
                                    echo "<td> $user->email </td>";
                                    echo "<td>";
                                    echo $user->getRoleFormatted();
                                    echo "</td>";
                                   
                                    echo
                                      "<td>
                                        
                                        <a href='/~team3/users/$user->id/edit' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-pencil\"></i> Edit </a>
                                        <a href='/~team3/users/delete/$user->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-trash\"></i> Delete </a>
                                      </td>";
                                }
                            ?>
                              <!-- Loop through the db -->
                              @foreach ($users as $userToShow)
                                @if ( Auth::user()->hasRole($userToShow->getRole()) && Auth::user()->getRole() != $userToShow->getRole()) 
                                 <tr>
                                
                                    <?php printUser($userToShow); ?>
                                 
                                    
                                </tr>
                                
                                @endif
                              @endforeach
                              
                           </tbody>
                          </table>
                         </div>
                        </div>
                      </div>
                     </div>
                    </div>
@section('scripts')
    <!-- Datatables -->
    <!-- this is not the way this is supposed to be done. php tags in a view is frowned upon but it works -->

    <script>
        $('#datatable').dataTable();
        $.extend( $.fn.dataTable.defaults, {
            responsive: true
        } );
    </script>




    <script src="{{asset('js/addToCart.js')}}"></script>
    <script src="{{asset('js/alertFade.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <!-- PNotify -->
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <!-- /PNotify -->

@stop

@stop
