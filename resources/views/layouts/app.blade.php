<!DOCTYPE html>

<?php
#UPDATED FOR LIVE SERVER JS 11/16/16
  use App\Provider;
  
use Illuminate\Support\Facades\Cache;
?>

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title><?php echo $__env->yieldContent('title'); ?></title>


  <!-- Bootstrap -->
  <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- Other Fonts -->
  <link rel="stylesheet" href="{{asset('css/fontello/css/fontello.css')}}">
  <!-- jVectorMap -->
  <link href="{{asset('css/maps/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet"/>

  <!-- Custom Theme Style -->
  <link href="{{asset('css/custom.css')}}" rel="stylesheet">
  <link href="{{asset('css/layout.css')}}" rel="stylesheet">

  <?php echo $__env->yieldContent('head'); ?>


</head>

<body class="nav-md" id="app-layout">

<!-- Prevents any user not logged in from seeing navigation tools -->
<?php if(Auth::user() !== null): ?>
<?php if(Auth::user()->hasRole('worker') || Auth::user()->hasRole('GA') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('superAdmin')  ): ?>
<div class="container body">
  <div class="main_container">
    <div class="col-md-3 left_col">
      <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
          <a href="{{url('home')}}" class="site_title">
            <img id="shield_logo" src="{{asset('images/shield_logo1.jpg')}}" alt="ETSU Logo" style="height: 100%;"/>
            <span id="social">Social Work</span>
          </a>
        </div>

        <div class="clearfix"></div>



        <!-- menu profile quick info -->
        <div class="profile">
          <div class="profile_pic">
            <img src="{{asset('images/PH.png')}}" alt="..." class="img-circle profile_img">
          </div>
          <div class="profile_info">
            <h2><?php if(Auth::user() !== null)
                echo e(Auth::user()->name); ?></h2>
            <? ?>
          </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <h3>&nbsp;</h3>
            <ul class="nav side-menu">
              <li><a href="{{url('providers')}}" role="button" class="btn btn-success"> Search </a></li>
             
              <li><a href="<?php echo e(url('/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>

              <?php
                    $count = 0;
                    $OodCount = 0;
              foreach(Provider::get() as $provider)
              {
                $id = ($provider->id);
                if(cache::has($id))
                {
                  $count = $count +1;
                }
                if($provider->flag_out_of_date)
                {
                  $OodCount = $OodCount +1;
                }
              }

               echo "<li><a href=\" " . e(url('/cart'))   . "\"><i class=\"fa fa-shopping-cart\"></i> PDF Cart &nbsp;";
                  if($count > 0)
                    {
                      echo "<div id='favs' style='float: right'> <span  class=\"label label-warning\">" . $count . " </span> </div></a></li> ";

                    }
                    else
                      {
                        echo " <div id='favs' style='float: right'></div> </a></li>";
                      }?>

              <?php if(Auth::user()->hasRole('worker')): ?>
              <li><a><i class="fa fa-user-md"></i>Providers<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="<?php echo e(url('/providers')); ?>">View Providers</a></li>
                  <?php if(Auth::user()->hasRole('GA')): ?>
                  <li><a href="{{url('providers/add')}}">Add New Provider</a></li>
                  <?php endif; ?>
                  <?php if(Auth::user()->hasRole('GA')): ?>
                    <li><a href="{{url('providers/archive')}}">Archived Providers</a></li>
                  <li><a href="{{url('providers/flagged')}}">View Out-Of-Date Providers&nbsp;
                      @if($OodCount > 0)
                      <span class="label label-warning">{{$OodCount}} </span>  </a></li>
                      @else
                      </a></li>
                      @endif



                  <?php endif; ?>
                </ul>
              </li>
              <?php endif; ?>


              <?php if(Auth::user()->hasRole('GA')): ?>
              <li><a><i class="fa fa-files-o"></i>Resources<span class="fa fa-chevron-down"/></a>
                <ul class="nav child_menu">
                  <li><a  href="{{url('resource')}}">View Resources</a></li>
                  <li><a  href="{{url('resources/add')}}">Add New Resource</a></li>
                  <li><a  href="{{url('resources/link')}}">Link Resource to Provider</a></li>
                </ul>
              </li>

              <?php endif; ?>
            </ul>
          </div>

          <?php if(Auth::user()->hasRole('GA')): ?>
          <div class="menu_section">
            <h3>Admin Settings</h3>
            <ul class="nav side-menu">
              <li><a><i class="fa fa-users"></i>Users<span class="fa fa-chevron-down"/></a>
                <ul class="nav child_menu">
                  <li><a  href="<?php echo e(url('/users')); ?>">Manage Users</a></li>
                  <li><a  href="{{url('users/add')}}">Add New User</a></li>
                </ul>
              </li>
              <li><a><i class="fa fa-bullhorn"></i>Temporary Events<span class="fa fa-chevron-down"/></a>
                <ul class="nav child_menu">
                  <li><a  href="<?php echo e(url('/temp')); ?>">Manage Events</a></li>
                  <li><a  href="{{url('temp/add')}}">Add New Event</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <?php endif; ?>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->

        <!-- <div class="sidebar-footer hidden-small">
          <a data-toggle="tooltip" class="footerOption" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
          </a>
          <a data-toggle="tooltip" class="footerOption" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
          </a>
          <a data-toggle="tooltip" class="footerOption" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
          </a>
          <a data-toggle="tooltip" class="footerOption" data-placement="top" href="" title="Logout">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
          </a>
        </div>-->
        <!-- /menu footer buttons -->
      </div>
    </div>

    <!-- top navigation -->

    <div id='top_nav' class="top_nav">


      <div class="nav_menu">


        <nav class="" role="navigation">


          <div class="nav toggle">
            <a id="menu_toggle" class="nav_bars"><i class="fa fa-bars"></i></a>

          
          </div>
         
          <ul class="nav navbar-nav navbar-right">
            <li class="">
              <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <img style="background-color: #ffffff !important;" src="{{asset('images/PH.png')}}" alt="">
                <span ><?php if(Auth::user() !== null)echo e(Auth::user()->name); ?></span>
              </a>
              <ul class="dropdown-menu dropdown-usermenu pull-right">
                <li><a href="<?php echo e(url('/profile')); ?>">  Profile</a>
                </li>
                <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                </li>
              </ul>
            </li>
            <li role="presentation">
              <a href="{{url('cart')}}" aria-expanded="false">
                <i class="fa fa-shopping-cart fa-2x">&nbsp;&nbsp;
                  <?php
                  //Uncommenting will display a badge icon next to shopping cart on the top nav bar
                  //if($count > 0)
    //              {
  //                  echo "<div id='favs2' style='float: right'> <span  class=\"label label-warning\">" . $count . " </span> </div> ";
//
            //}
            //else
            //{
            //echo " <div id='favs2' style='float: right'></div> ";
            //}?> </i>
              </a>
            </li>
          </ul>
        </nav>
      </div>


    </div>

    <!-- /top navigation -->

  <?php endif; ?>
  <?php endif; ?>
  <!-- page content -->

    <div class="right_col" role="main">

      <br />
      <div class="">

        <?php echo $__env->yieldContent('content'); ?>
      </div>
    </div>
    <!-- /page content -->
  <?php if(Auth::user() !== null): ?>
  <?php if(Auth::user()->hasRole('worker') || Auth::user()->hasRole('GA') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('superAdmin') ||  Auth::user()->hasRole('waiting') ): ?>
  <!-- footer content -->
<footer>
  <div class="text-center">
    <img src="{{asset('images/footer_logo_2.png')}}" width="30%" height="auto" />
    <!--Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>-->
    <br />
    <br />
    <table style="width:95%;" align="center">
      <tr>
        <td align="left">Human Services Program</td>
      </tr>
      <tr>
        <td align="left">Department of Counseling & Human Services</td>
      </tr>
      <tr>
        <td align="left">PO Box 70701</td>
        <td align="right">East Tennessee State University</td>
      </tr>
      <tr>
        <td align="left">Johnson City, TN 37614 </td>
        <td align="right">PO Box 70300 | Johnson City, TN 37614</td>
      </tr>
      <tr style="width:95%; border-bottom: 1px solid #FFC72C" align="center">
        <td align="left">423-439-7692</td>
        <td align="right">423-439-1000 | info@etsu.edu</td>
      </tr>
      <tr>
        <td><br /></td>
      </tr>
    </table>
    <br />
    <br />
    <p align="center">
      East Tennessee State University  <a href="http://a.cms.omniupdate.com/10?skin=etsu&account=east-tennessee-state&site=ETSU_Web_Linux&action=de&path=/coe/chs/humanservices/default.pcf"  style="color: white">©</a>   2016  All Rights Reserved    |   
      <a href="http://www.etsu.edu/etsuhome/documents/webprivacystatement.pdf"  style="color: white">Privacy Policy</a>
    </p>
  </div>


      <div class="clearfix"></div>
</footer>
    <!-- /footer content -->
  </div>
</div>

@section('scripts')
<!-- jQuery -->

<script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>

<?php echo $__env->yieldContent('scripts'); ?>
<!-- Bootstrap -->
<script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
<!-- Chart.js -->
<script src="{{asset('vendors/Chart.js/dist/Chart.min.js')}}"></script>
<!-- jQuery Sparklines -->
<script src="{{asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{asset('js/moment/moment.min.js')}}"></script>
<script src="{{asset('js/datepicker/daterangepicker.js')}}"></script>

<!-- Custom Theme Scripts -->
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>

<?php echo $__env->yieldContent('scripts'); ?>
<!-- Datatables -->
<script>
  $(document).ready(function() {
    var handleDataTableButtons = function() {
      if ($("#datatable-buttons").length) {
        $("#datatable-buttons").DataTable({
          dom: "Bfrtip",
          buttons: [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ],
          responsive: true
        });
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();
    //this is the line that is creating the second search bar.
    //$('#datatable').dataTable();
    $('#datatable-keytable').DataTable({
      keys: true
    });

    $('#datatable-responsive').DataTable();

    $('#datatable-scroller').DataTable({
      ajax: "js/datatables/json/scroller-demo.json",
      deferRender: true,
      scrollY: 380,
      scrollCollapse: true,
      scroller: true
    });

    var table = $('#datatable-fixed-header').DataTable({
      fixedHeader: true
    });

    TableManageButtons.init();
  });
</script>
<!-- /Datatables -->

<!-- jQuery Sparklines -->
<script>
  $(document).ready(function() {
    $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
      type: 'bar',
      height: '125',
      barWidth: 13,
      colorMap: {
        '7': '#a1a1a1'
      },
      barSpacing: 2,
      barColor: '#26B99A'
    });

    $(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
      type: 'bar',
      height: '40',
      barWidth: 8,
      colorMap: {
        '7': '#a1a1a1'
      },
      barSpacing: 2,
      barColor: '#26B99A'
    });

    $(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
      type: 'line',
      height: '40',
      width: '200',
      lineColor: '#26B99A',
      fillColor: '#ffffff',
      lineWidth: 3,
      spotColor: '#34495E',
      minSpotColor: '#34495E'
    });
  });
</script>
<!-- /jQuery Sparklines -->

<!-- Doughnut Chart -->
<script>
  $(document).ready(function() {
    var canvasDoughnut,
            options = {
              legend: false,
              responsive: false
            };

    new Chart(document.getElementById("canvas1i"), {
      type: 'doughnut',
      tooltipFillColor: "rgba(51, 51, 51, 0.55)",
      data: {
        labels: [
          "Symbian",
          "Blackberry",
          "Other",
          "Android",
          "IOS"
        ],
        datasets: [{
          data: [15, 20, 30, 10, 30],
          backgroundColor: [
            "#BDC3C7",
            "#9B59B6",
            "#E74C3C",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#CFD4D8",
            "#B370CF",
            "#E95E4F",
            "#36CAAB",
            "#49A9EA"
          ]

        }]
      },
      options: options
    });

    new Chart(document.getElementById("canvas1i2"), {
      type: 'doughnut',
      tooltipFillColor: "rgba(51, 51, 51, 0.55)",
      data: {
        labels: [
          "Symbian",
          "Blackberry",
          "Other",
          "Android",
          "IOS"
        ],
        datasets: [{
          data: [15, 20, 30, 10, 30],
          backgroundColor: [
            "#BDC3C7",
            "#9B59B6",
            "#E74C3C",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#CFD4D8",
            "#B370CF",
            "#E95E4F",
            "#36CAAB",
            "#49A9EA"
          ]

        }]
      },
      options: options
    });

    new Chart(document.getElementById("canvas1i3"), {
      type: 'doughnut',
      tooltipFillColor: "rgba(51, 51, 51, 0.55)",
      data: {
        labels: [
          "Symbian",
          "Blackberry",
          "Other",
          "Android",
          "IOS"
        ],
        datasets: [{
          data: [15, 20, 30, 10, 30],
          backgroundColor: [
            "#BDC3C7",
            "#9B59B6",
            "#E74C3C",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#CFD4D8",
            "#B370CF",
            "#E95E4F",
            "#36CAAB",
            "#49A9EA"
          ]

        }]
      },
      options: options
    });
  });
</script>
<!-- /Doughnut Chart -->

<!-- bootstrap-daterangepicker -->
<script type="text/javascript">
  $(document).ready(function() {

    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    };

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2012',
      maxDate: '12/31/2015',
      dateLimit: {
        days: 60
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'left',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Submit',
        cancelLabel: 'Clear',
        fromLabel: 'From',
        toLabel: 'To',
        customRangeLabel: 'Custom',
        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        firstDay: 1
      }
    };
    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#reportrange').daterangepicker(optionSet1, cb);
    $('#reportrange').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });
    $('#options1').click(function() {
      $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
    });
    $('#options2').click(function() {
      $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
    });
    $('#destroy').click(function() {
      $('#reportrange').data('daterangepicker').remove();
    });
  });
</script>

<!-- /bootstrap-daterangepicker -->


<?php endif; ?>
<?php endif; ?>
</body>
</html>
