@extends('layouts.app')

@section('title')
    Archive Providers List
@stop

@section('head')
    <!-- PNotify -->
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
@stop

@section('content')
    <?php


    function printProvider($provider)
    {




        if($provider->flag_out_of_date)
        {echo "<td> Yes </td>";}
        else
        {echo "<td> No </td>";}
        echo "<td> $provider->name </td>";

        $formattedPhone = $provider->formatPhone();

        echo "<td> $formattedPhone </td>";
        echo "<td> $provider->description </td>";
        //Note: By coincidence, all the descriptions are short right now,
        //but there will probably be long in the future
        echo "<td> ";
        foreach($provider->resources as $resource)
        {
            echo $resource->type;
            echo " - ";
            echo $resource->subtype;
            echo "</br>";
        }
        echo " </td>";
        echo "<td> $provider->county </td>";
        echo "<td> <a href=\"$provider->id\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> View </a>";
        if (Auth::user()->hasRole('superAdmin'))
        {

            echo "  <a href=\"restoreArchive/$provider->id\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-folder\"></i> Restore </a>";
        }


    }

    ?>


     <div id="flashDiv" class="clearfix"><br><br>@include('flash::message')</div>

    <div id="archived" class="">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Archived Providers</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <table id="datatable" class="table table-striped dt-responsive  " cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Flagged</th>
                                <th>Name</th>
                                <th>Phone number</th>
                                <th>Description</th>
                                <th>Resources</th>
                                <th>County</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($providers as $provider)
                                <tr>
                                    <?php printProvider($provider); ?>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <!-- Datatables -->
    <!-- this is not the way this is supposed to be done. php tags in a view is frowned upon but it works -->
    <?php
    if(isset($searchTerm))
    {
        echo "<script>

            $.extend( $.fn.dataTable.defaults, {
                responsive: true
            } );
            $(document).ready(function() {
            $('#datatable').DataTable({\"search\": {\"search\": \"$searchTerm\"}});
});
            </script>";
    }
    else
    {
        echo "<script>

            $.extend( $.fn.dataTable.defaults, {
                responsive: true
            } );
            $(document).ready(function() {
            $('#datatable').DataTable();
          });
            </script>";
    }
    ?>
    <script src="{{asset('js/restoreArchived.js')}}"></script>
    <script src="{{asset('js/alertFade.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <!-- PNotify -->
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <!-- /PNotify -->
@stop