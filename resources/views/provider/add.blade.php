@extends('layouts.app')

@section('title')
Add New Provider
@stop

@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
    @include('flash::message')
  <div class="x_panel">
    <div class="x_title">
      <h2>Add Provider</h2>                
      <br>       
    </div>
    {!! Form::open(array('action' => array('ProviderController@addProvider'), 'class' => 'form-horizontal form-label-left')) !!}    
    <br>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="name" name="name" placeholder = "Enter Name" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street">Street
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="street" name="street" placeholder = "Enter Street" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="city" name="city" placeholder = "Enter City" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="county">County
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="county" name="county" placeholder = "Enter County" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zip">Zip Code
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="number" id="zip" name="zip" placeholder = "Enter Zip Code" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">State
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="form-control" name="state">
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District Of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option selected value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
          </select>
        </div>
      </div>

      <div id="phoneContainer" class="form-group"></div>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <input type="button" class="btn btn-primary" value="Add Phone" onClick="addPhone('phoneContainer')" />
          </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="email" id="email" name="email" placeholder = "Enter Email" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="web">Web Address
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="web" name="web" placeholder = "Enter Web Address" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_f_name">Contact First Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="contact_f_name" name="contact_f_name" placeholder = "Enter Contacts First Name" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_l_name">Contact Last Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="contact_l_name" name="contact_l_name" placeholder = "Enter Contact Last Name" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location Constraint
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="location" name="location" placeholder = "Enter Location Constraint" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="population">Population Constraint
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="population" name="population" placeholder = "Enter Population Constraint" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="office_hours">Office Hours
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="office_hours" name="office_hours" placeholder = "Enter Office Hours" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" placeholder = "Enter Description" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="intake">Client Intake
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="intake" name="intake" placeholder = "By appointment/walk-in" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fees">Fee
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="fees" name="fees" placeholder = "Enter Fees" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div id="resourceContainer" class="form-group"></div>
      <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <input type="button" class="btn btn-primary" value="Add Resource" onClick="addResource('resourceContainer', resourceContents)" />
          </div>
      </div>

      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-success" name="save" value="Save">
            <input type="submit" formnovalidate class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

{!! Form::close() !!} 

    </div>
  </div>

@section('scripts')
    <script src="{{asset('js/alertFade.js')}}"></script>
    <script src="{{asset('js/ProviderRepeaters.js')}}"></script>

    <script>
        var resourceContents =
            <?php
                foreach($resources as $resource)
                {
                    echo "'<option value=\"" . $resource->id . "\">$resource->subtype</option>' +";
                }
            ?>
            "";
    </script>


@endsection
@stop
