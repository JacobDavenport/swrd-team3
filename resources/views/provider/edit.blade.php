@extends('layouts.app')

@section('title')

Edit Provider
 
<?php
    echo $provider->name;
?>
@stop
<!--
This section needs to be refactored to match the other pages layout, and design.
-->
@section('content')
<!--
This is copied from the User Edit page to make sure that is looks the same. It will need to be changed for the things in the provider class.
-->
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Edit Provider</h2>                
      <br>       
    </div>           
    {!! Form::open(array('action' => array('ProviderController@updateProvider',$provider->id), 'class' => 'form-horizontal form-label-left')) !!}    
    <br>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="hidden" id="id"  value = "{{$provider->id}}" required="required" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="name" name="name" value = "{{ $provider->name}}" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street">Street 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="street" name="street" value = "{{ $provider->street}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">City  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="city" name="city" value = "{{ $provider->city}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="county">County  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="county" name="county" value = "{{ $provider->county}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zip">Zip  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="zip" name="zip" value = "{{ $provider->zip}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">State 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="state" name="state" value = "{{ $provider->state}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div id="phoneContainer" class="form-group"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <input type="button" class="btn btn-primary" value="Add Phone" onClick="addPhone('phoneContainer')" />
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="email" name="email" value = "{{ $provider->email}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="web">Web Address 
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="web" name="web" value = "{{ $provider->web}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_f_name">Contact First Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="contact_f_name" name="contact_f_name" value = "{{ $provider->contact_f_name}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_l_name">Contact Last Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="contact_l_name" name="contact_l_name" value = "{{ $provider->contact_l_name}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="location" name="location" value = "{{ $provider->location}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="population">Population  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="population" name="population" value = "{{ $provider->population}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="office_hours">Office Hours  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="office_hours" name="office_hours" value = "{{ $provider->office_hours}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" value = "{{ $provider->description}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="intake">Intake  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="intake" name="intake" value = "{{ $provider->intake}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="feed">Fees  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="fees" name="fees" value = "{{ $provider->fees}}" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div id="resourceContainer" class="form-group"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <input type="button" class="btn btn-primary" value="Add Resource" onClick="addResource('resourceContainer', resourceContents)" />
        </div>
      </div>
            
      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-success" name="save" value="Save Changes">
            <input type="submit" class="btn btn-danger" name="delete" value="Delete">
            <input type="submit"  class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

{!! Form::close() !!}

@section('scripts')
  <script src="{{asset('js/ProviderRepeaters.js')}}"></script>

  <script>
    //Setup the repeating fields using data from the database

    //Setup the resources combo box data
    var resourceContents =
            <?php
              foreach($resources as $resource)
              {
                echo "'<option value=\"" . $resource->id . "\">$resource->subtype</option>' +";
              }
            ?>
            '';

    if($("#phoneContainer").children().length == 0){
      //Display the existing phones
      var idx = 0;
      <?php
      foreach($provider->phones as $p)
      {
        echo "idx = addPhone('phoneContainer');\n";
        echo "fillPhone(idx, '$p->phone', '$p->note')\n";
      }
      ?>
    }

    if($("#resourceContainer").children().length == 0){
      //Display existing resources
      var idx = 0
      <?php
      foreach($provider->resources as $r)
      {
        echo "idx = addResource('resourceContainer', resourceContents);\n";
        echo "selectResource(idx, '$r->id')\n";
      }
      ?>
    }

  </script>


@endsection

    </div>
  </div>
@stop