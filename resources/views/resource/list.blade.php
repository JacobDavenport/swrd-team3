@extends('layouts.app')

@section('title')
    Resources List
@stop
@section('head')
    <!-- PNotify -->
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
@stop

@section('content')


    <!-- page content -->


    <div class="">

        <div class="clearfix">@include('flash::message')</div>
        <div class="row">


            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <div class="x_title">
                        <h2>Resources</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table class="table table-striped projects">
                            <table id="datatable" class="table table-striped dt-responsive " cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th style="width: 20%">Type</th>
                                    <th>Sub-type</th>
                                    <th>Description</th>
                                    <th>Settings</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Query the RESOURCE database table -->
                                <?php
                                function printResource($resource)
                                {
                                    echo "<td> $resource->type </td>";
                                    echo "<td> $resource->subtype </td>";
                                    echo "<td> $resource->description </td>";
                                    //echo "<td><input type=\"checkbox\" name=\"Needs Attention\"></td>";
                                    //todo: remove first <a> tag below
                                    // echo

                                    if (Auth::user()->hasRole('GA'))
                                    {

                                        echo   " <td> <a href='resources/edit/$resource->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-pencil\"></i> Edit </a>";
                                    }

                                    if (Auth::user()->hasRole('admin'))
                                    {

                                        echo    "<a href='resources/delete_/$resource->id' class=\"btn btn-primary btn-xs\"><i class=\"fa fa-trash-o\"></i> Delete </a>";
                                    }
                                    echo "</td>";
                                }




                                ?>
                                <!-- Loop through the db -->
                                @foreach ($resources as $r)
                                    <tr>
                                        <?php printResource($r); ?>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
    <!-- TODO: this next section needs to be in a resource.blade.php file
         under the layouts folder instead of the following hard coded -->
@stop

@section('scripts')
    <!-- Datatables -->
    <!-- this is not the way this is supposed to be done. php tags in a view is frowned upon but it works -->

    <script>
        $('#datatable').dataTable();
        $.extend( $.fn.dataTable.defaults, {
            responsive: true
        } );
    </script>




    <script src="{{asset('js/addToCart.js')}}"></script>
    <script src="{{asset('js/alertFade.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <!-- PNotify -->
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <!-- /PNotify -->

@stop