@extends('layouts.app')

@section('title')
    Link Resource
@stop

@section('content')
    <div class="clearfix">    @include('flash::message')</div>
    <div class="row">

        <div class="col-md-6 col-md-offset-2 col-sm-12 col-xs-12">

            <div class="x_panel">
                <div class="x_title">
                    <h2>Link Resource to Provider</h2>
                    <br>
                </div>
                <div class="x_content">
                    <br>
                    <form action="{{url('resources/link')}}" method="post" id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Resource Type <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select required="required" id="resourceId" name="resourceId" class="form-control col-md-7 col-xs-12">
                                    <?php
                                    foreach($resources as $resource)
                                    {

                                        echo "<option value='" . $resource->id . "'>$resource->subtype</option>";
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">Provider <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select required="required" id="providerId" name="providerId" class="form-control col-md-7 col-xs-12">
                                    <?php
                                    foreach($providers as $provider)
                                    {

                                        echo "<option value='" . $provider->id . "'>$provider->name</option>";
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">

                                <input type="submit" class="btn btn-success" name="save" value="Save">
                                <input type="submit" formnovalidate  class="btn btn-primary" name="cancel" value="Cancel">


                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@section('scripts')
        <script src="{{asset('js/alertFade.js')}}"></script>
@endsection
@stop