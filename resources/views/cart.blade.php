@extends('layouts.app')
@section('head')
    <!-- PNotify -->
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
@stop
@section('title')
PDF Cart
@stop

@section('content')

<div class="clearfix"><br><br>  @include('flash::message')</div>
     <div class="row">


      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Cart</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

          <table id="datatable" class="table table-striped nowrap">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Phone number</th>
                  <th>Remove from Cart</th>
                </tr>
              </thead>
             <tbody>
             @if(isset($providerArray))
             @foreach($providerArray as $pdfitem)
                 <tr>
                    <td>
                        <?php echo $pdfitem->name; ?>
                    </td>
                    <td>
                        <?php echo $pdfitem->description; ?>
                    </td>
                    <td>
                        <?php echo $pdfitem->formatPhone(); ?>
                    </td>
                    <td style="text-align: center">
                        <?php echo "<a href=" . url('cart/remove/' . $pdfitem->id) . " <button type=\"button\" class=\"btn btn-round btn-info\"><i class=\"fa fa-minus\"></i></button></a>"; ?>
                    </td>
                 </tr>
             @endforeach
             @else
             <tr><td><b>Cart is empty</b></td></tr>
             @endif
       </tbody>
      </table>
      <table>
        <tr>
            <td>
                <a href="{{url('cart/clearCart')}}" class="btn btn-primary btn-xs">Clear Cart</a>
            </td>
            <td>
                <a onclick=popUpWindow() class="btn btn-primary btn-xs">Print Cart</a>
            </td>
        </tr>
      </table>
     </div>
    </div>
  </div>
 </div>
</div>

@stop
@section('scripts')

   <script src="{{asset('js/alertFade.js')}}"></script>
    
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <!-- PNotify -->
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <!-- /PNotify -->
 <script>
        function popUpWindow(){window.open("/~team3/cart/PDF", "Cart Print View");}
    </script>
@stop