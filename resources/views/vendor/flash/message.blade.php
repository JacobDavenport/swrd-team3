@if (session()->has('flash_notification.message'))
    @if (session()->has('flash_notification.overlay'))
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => session('flash_notification.title'),
            'body'       => session('flash_notification.message')
        ])
    @else
        <div id="flash" class="alert
                    alert-{{ session('flash_notification.level') }}
                    {{ session()->has('flash_notification.important') ? 'alert-important' : '' }}" 
                    style="
                    font-size: large;
                    color: #FFFFFF;
                    background-color: #041e42;
                    border-color: #FFFFFF;
                    border-width: 0px;
                    position: fixed;
                    left: 10;
                    right: 20;
                    box-shadow:0 0 0 3px #ffc72c;
                    height: auto;
                    width: 80%;
                    z-index: 100; "
                    >
            <button type="button"
                    class="close"
                    data-dismiss="alert"
                    style="color:#FFFFFF;"

            >&times;</button>
            @if(session()->has('flash_notification.important'))
                <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true"
                >&times;</button>
            @endif

            {!! session('flash_notification.message') !!}
        </div>
    @endif
@endif

