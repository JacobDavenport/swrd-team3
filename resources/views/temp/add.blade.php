@extends('layouts.app')

@section('title')
Add New Temporary Event
@stop

@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
    @include('flash::message')
  <div class="x_panel">
    <div class="x_title">
      <h2>Add Temporary Event</h2>
      <br>       
    </div>
    {!! Form::open(array('action' => array('TempController@addTemp'), 'class' => 'form-horizontal form-label-left')) !!}    
    <br>
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="name" name="name" placeholder = "Enter Name" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" placeholder = "Enter Description" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="date" id="date" name="date" placeholder = "20XX-MM-DD" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="time">Starting Time
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="time" id="start_time" name="start_time" placeholder = "Enter Starting Time (XX:XX)" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="time">Ending Time
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="time" id="end_time" name="end_time" placeholder = "Enter Starting Time (XX:XX)" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="location">Location
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="location" name="location" placeholder = "Enter Location" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="expiration_date">Expiration Date
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="date" id="expiration_date" name="expiration_date" placeholder = "20XX-MM-DD" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event_contact">Event Contact
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="event_contact" name="event_contact" placeholder = "Enter Event Contact" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="phone" name="phone" placeholder = "Enter Phone" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-success" name="save" value="Save">
            <input type="submit" formnovalidate class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

{!! Form::close() !!} 

    </div>
  </div>

@section('scripts')
    <script src="{{asset('js/AlertFade.js')}}"></script>
@endsection
@stop
