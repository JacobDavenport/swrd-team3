@extends('layouts.app')

@section('title')



<?php
    echo $notification->name;
?>
@stop
<!--
This section needs to be refactored to match the other pages layout, and design.
-->
@section('content')
 
<!--
This is copied from the User Edit page to make sure that is looks the same. It will need to be changed for the things in the provider class.
-->
<div class="col-md-12 col-sm-12 col-xs-12">
       @include('flash::message')
  <div class="x_panel">
    <div class="x_title">
      <h2>Are you sure want to delete the item?</h2>                
      <br>       
    </div>           
    {!! Form::open(array('action' => array('TempController@deleteTemp',$notification->id), 'class' => 'form-horizontal form-label-left')) !!}    
    <br>
   

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name  
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="name" name="name" value = "{{$notification->name}}" required="required" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" id="description" name="description" value = "{{$notification->description}}" required="required" class="form-control col-md-7 col-xs-12" readonly>
        </div>
      </div>

    
            
      <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <input type="submit" class="btn btn-danger" name="delete" value="Delete">
            <input type="submit"  class="btn btn-primary" name="cancel" value="Cancel">
          </div>
        </div>
      </div>

{!! Form::close() !!} 

    </div>
  </div>
  

@stop