@extends('layouts.app')

@section('title')
Temporary Events List
@stop

@section('head')
<!-- PNotify -->
<link href="{{asset('css/tempNotification.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.buttons.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/pnotify/dist/pnotify.nonblock.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
@stop



@section('content')
<?php
require(app_path('t_notifications.blade.php'));
?>

@include('flash::message')
<br>
<br>
  <div class="">
  <div class="clearfix"></div>
     <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Temporary Events</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <table id="datatable" class="table table-striped dt-responsive " cellspacing="0" width="100%">
            <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Location</th>
                  <th>Expiration Date</th>
                  <th>Event Contact</th>
                  <th>Contact Phone</th>
                  <th>Options</th>
                </tr>
              </thead>
             <tbody>

                     <?php require(app_path('printNotification.php')); ?>

       </tbody>
      </table>
     </div>
    </div>
  </div>
 </div>
</div>
@section('scripts')
    <!-- Datatables -->
    <!-- this is not the way this is supposed to be done. php tags in a view is frowned upon but it works -->

    <script>
        $('#datatable').dataTable();
        $.extend( $.fn.dataTable.defaults, {
            responsive: true
        } );
    </script>



    <script src="{{asset('js/toggleDiv.js')}}"></script>
    <script src="{{asset('js/addToCart.js')}}"></script>
    <script src="{{asset('js/alertFade.js')}}"></script>
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
    <script src="{{asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <!-- PNotify -->
    <script src="{{asset('vendors/pnotify/dist/pnotify.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
    <script src="{{asset('vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
    <!-- /PNotify -->

@stop

@stop

