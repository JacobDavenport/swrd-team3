@extends('layouts.app')
<!--UPDATED FOR LIVE SERVER JS 11/16/16-->

@section('title')
SWRD Home
@stop
@section('content')
    <link href="{{asset('css/tempNotification.css')}}" rel="stylesheet">
<br>
<br>
<div class="container">
    <div class="row">
        <?php require(app_path('t_notifications.blade.php'));?>

        <div class=" col-md-offset-2 col-md-8 col-sm-8 col-xs-8" >
            <div class="x_panel">
                <div class="x_title">
                    <h2>Dashboard</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content" style="font-size:20px">

                    <div class="row text-center">
                        <!-- There is something going on with the first icon in the grid. When it is clicked nothing is happeneing. It does not matter which icon is first. Right now it is utility
                             but it could the rent, or whatever and it will not load the page. It must be something with the javascript. This also happens irregardless of the changes that were made
                             in the layouts.app file. -->
                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/utility')}}" class="searchMain"><i class="fa fa-lightbulb-o fa-5x" aria-hidden="true"></i><br><br>Utility</a></div>
                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/housing')}}" class="searchMain"><i class="fa fa-home fa-5x" aria-hidden="true"></i><br><br>Housing</a></div>
                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/food')}}" class="searchMain"><i class="fa fa-cutlery fa-5x" aria-hidden="true"></i><br><br>Food</a></div>
                    </div>
                    <div class="row text-center">
                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/legal')}}" class="searchMain"><i class="fa fa-gavel fa-5x" aria-hidden="true"></i><br><br>Legal</a></div>

                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/tax')}}" class="searchMain"><i class="fa fa-calculator fa-5x" aria-hidden="true"></i><br><br>Financial</a></div>



                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/transportation')}}" class="searchMain"><i class="fa fa-car fa-5x" aria-hidden="true"></i><br><br>Transportation</a></div>
                    </div>
                    <div class="row text-center">
                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/medical')}}" class="searchMain"><i class="fa fa-stethoscope fa-5x" aria-hidden="true"></i><br><br>Medical</a></div>
                        <div class="col-xs-12 col-md-4" ><a href="{{url('providers/clothing')}}" class="searchMain"><i class="demo-icon icon-t-shirt fa-5x" aria-hidden="true"></i><br>Clothing</a></div>

                        <div class="col-xs-12 col-md-4" style="padding:1em"><a href="{{url('providers/job')}}" class="searchMain"><i class="fa fa-building-o fa-5x" aria-hidden="true"></i><br><br>Employment</a></div>


                    </div>
                </div><!-- end div="x_content"-->
            </div>
        </div>
    </div>
    <div class>
    </div>

</div><!-- end div="container"-->
@section('scripts')
    <script src="{{asset('js/toggleDiv.js')}}"></script>
    <script src="{{asset('js/alertFade.js')}}"></script>
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>

@endsection

@endsection